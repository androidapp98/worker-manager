package il.co.techschool.workmanageapp.activities.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Admin on 10/15/2017.
 */

public final class Utils {
    private Utils() {

    }

    final static String mdbDateFormat = "dd/MM/yyyy";
    final static String mdbDateTimeFormat = "dd/MM/yyyy HH:mm:ss.sss";

    /**
     *
     * @param aDate
     * @return
     */
    public static Date StringToDate(String aDate) {

        SimpleDateFormat format = new SimpleDateFormat(mdbDateFormat);
        try {
            Date date = format.parse(aDate);
            return date;
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     *
     * @param aDate
     * @return
     */
    public static String DateToString(Date aDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(mdbDateFormat);
        try {

            String datetime = dateFormat.format(aDate);
            return datetime;
        } catch (Exception e) {
            return "";
        }

    }

    /**
     *
     * @param aDateTime
     * @return
     */
    public static Date StringToDateTime(String aDateTime) {
        if (aDateTime == "")
            return null;


        SimpleDateFormat format = new SimpleDateFormat(mdbDateTimeFormat);
        try {
            Date datetime = format.parse(aDateTime);
            return datetime;
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     *
     * @param aDateTime
     * @return
     */
    public static String DateTimeToString(Date aDateTime) {
        if (aDateTime == null) {
            return "";
        }else
        {
        SimpleDateFormat dateFormat = new SimpleDateFormat(mdbDateTimeFormat);
        try
        {

            String datetime = dateFormat.format(aDateTime);
            return datetime;
        } catch (Exception e)
        {
            return "";
        }
    }
      }


}
