package il.co.techschool.workmanageapp.activities.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import il.co.techschool.workmanageapp.R;
import il.co.techschool.workmanageapp.activities.adapters.AdapterCompanyProjects;
import il.co.techschool.workmanageapp.activities.common.AppWorkerPresence;


/**
 * Created by Admin on 1/19/2018.
 */

public class ActCompanyProjectsList extends AppCompatActivity {

    private ListView mlstCompanyProjects;

    private Context mContext;
    private AppWorkerPresence mAppWorkerPresence;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_company_workers_list);

        mAppWorkerPresence = (AppWorkerPresence) getApplication();
        mContext = this;
        initComponents();
    }

    /**
     *
     */
    @Override
    protected void onResume() {
        super.onResume();
        AdapterCompanyProjects adapterCompanyProjects;
        // start using the adapter after you have the data loading...
        adapterCompanyProjects = new AdapterCompanyProjects(mContext, mAppWorkerPresence.APP_INSTANCE.getArrayListCompanyProjects());
        mlstCompanyProjects.setAdapter(adapterCompanyProjects);
    }

    /**
     *
     */
    private void initComponents() {
        mlstCompanyProjects = (ListView) findViewById(R.id.lstCompanyProjects);
    }
}