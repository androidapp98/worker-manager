package il.co.techschool.workmanageapp.activities.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by Admin on 12/11/2017.
 */

public class CompanyWorkers {
    @SerializedName("db_id")
    @Expose
    private String dbId;
    @SerializedName("company_id")
    @Expose
    private Integer companyId;
    @SerializedName("worker_Id")
    @Expose
    private Integer workerId;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("salary")
    @Expose
    private Integer salary;

    /**
     *
     */
    public CompanyWorkers() {

    }

    /**
     *
     * @param aDbId
     * @param aCompanyId
     * @param aWorkerId
     * @param aStatus
     * @param aSalary
     */
    public CompanyWorkers(String aDbId, Integer aCompanyId, Integer aWorkerId, Integer aStatus, Integer aSalary) {
        dbId = aDbId;
        companyId = aCompanyId;
        workerId = aWorkerId;
        status = aStatus;
        salary = aSalary;

    }

    /**
     *
     * @return
     */
    public String getDbId() {
        return dbId;
    }

    /**
     *
     * @param aDbId
     */
    public void setDbId(String aDbId) {
        dbId = aDbId;
    }

    /**
     *
     * @return
     */
    public Integer getCompanyId() {
        return companyId;
    }

    /**
     *
     * @param aCompanyId
     */
    public void setCompanyId(Integer aCompanyId) {
        companyId = aCompanyId;
    }

    /**
     *
     * @return
     */
    public Integer getWorkerId() {
        return workerId;
    }

    /**
     *
     * @param aWorkerId
     */
    public void setWorkerId(Integer aWorkerId) {
        workerId = aWorkerId;
    }

    /**
     *
     * @return
     */
    public Integer getStatus() {
        return status;
    }

    /**
     *
     * @param aStatus
     */
    public void setStatus(Integer aStatus) {
        status = aStatus;
    }

    /**
     *
     * @return
     */
    public Integer getSalary() {
        return salary;
    }

    /**
     *
     * @param aSalary
     */
    public void setSalary(Integer aSalary) {
        salary = aSalary;
    }

    /**
     *
     * @param aCompanyWorkers
     */
    public void updateFrom(CompanyWorkers aCompanyWorkers) {
        dbId = aCompanyWorkers.dbId;
        companyId = aCompanyWorkers.companyId;
        workerId = aCompanyWorkers.workerId;
        status = aCompanyWorkers.status;
        salary = aCompanyWorkers.salary;

    }

}
