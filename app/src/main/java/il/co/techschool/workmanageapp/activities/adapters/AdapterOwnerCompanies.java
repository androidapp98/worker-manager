package il.co.techschool.workmanageapp.activities.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import il.co.techschool.workmanageapp.R;
import il.co.techschool.workmanageapp.activities.activities.ActCompany;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListOwnerCompanies;
import il.co.techschool.workmanageapp.activities.common.AppWorkerPresence;
import il.co.techschool.workmanageapp.activities.entities.Company;
import il.co.techschool.workmanageapp.activities.entities.CompanyProjects;
import il.co.techschool.workmanageapp.activities.entities.CompanyWorkers;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by Admin on 12/12/2017.
 */

public class AdapterOwnerCompanies extends BaseAdapter {

    private Context mContext;
    private ArrayListOwnerCompanies mArrayListOwnerCompanies;

    public AdapterOwnerCompanies(Context aContext, ArrayListOwnerCompanies aArrayListOwnerCompanies) {
        mArrayListOwnerCompanies = aArrayListOwnerCompanies;
        mContext = aContext;

    }

    /**
     *
     * @return
     */
    @Override
    public int getCount() {
        return mArrayListOwnerCompanies.size();
    }

    /**
     *
     * @param i
     * @return
     */
    @Override
    public Object getItem(int i) {
        return mArrayListOwnerCompanies.getLoaded();
    }

    /**
     *
     * @param i
     * @return
     */
    @Override
    public long getItemId(int i) {
        return 0;
    }

    /**
     *
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View resultView;
        LayoutInflater layoutInflater;
        final Company currentCompany;

        ImageView imgCompanyIcon;
        TextView txtCompanyName;
        ImageButton imgBtnCompany;

        if (convertView == null) {
            layoutInflater = (LayoutInflater) AppWorkerPresence.APP_INSTANCE.getSystemService(LAYOUT_INFLATER_SERVICE);

            // create the custom layout to show the "companies"
            resultView = layoutInflater.inflate(R.layout.lo_item_company, null);

            // find the objects inside the custom layout...
            imgCompanyIcon = (ImageView) resultView.findViewById((R.id.imgItmCompanyIcon));
            txtCompanyName = (TextView) resultView.findViewById((R.id.txtItmCompanyName));
            imgBtnCompany = (ImageButton) resultView.findViewById((R.id.imgBtnItmCompany));

            // add all the "objects" to the "tag" list
            //resultView.setTag( R.id.<item it>, itemObject);

            resultView.setTag(R.id.imgItmCompanyIcon, imgCompanyIcon);
            resultView.setTag(R.id.txtItmCompanyName, txtCompanyName);
            resultView.setTag(R.id.imgBtnItmCompany, imgBtnCompany);

        } else {
            resultView = convertView;

            // extract all the pointers to the objects from the "tag" list.
            imgCompanyIcon = (ImageView) resultView.getTag(R.id.imgItmCompanyIcon);
            txtCompanyName = (TextView) resultView.getTag(R.id.txtItmCompanyName);
            imgBtnCompany = (ImageButton) resultView.getTag(R.id.imgBtnItmCompany);
        }
        currentCompany = mArrayListOwnerCompanies.get(position);

        // update custom items in the view with the data from the "currentWorker";
        txtCompanyName.setText(currentCompany.getCompanyName());
        byte[] icon = currentCompany.getCompanyIcon();
        if (icon != null)
        {
            Bitmap bitmap = BitmapFactory.decodeByteArray(icon, 0, icon.length);
            imgCompanyIcon.setImageBitmap(bitmap);
        }

        txtCompanyName.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param v
             */
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext, " get click", Toast.LENGTH_SHORT).show();

                Bundle dataBundle = new Bundle();
                dataBundle.putString("id", currentCompany.getDbId());
                Intent intent = new Intent(mContext, ActCompany.class);
                intent.putExtras(dataBundle);
                mContext.startActivity(intent);

            }
        });
        try {
            imgBtnCompany.setOnClickListener(new View.OnClickListener() {
                /**
                 *
                 * @param v
                 */
                @Override
                public void onClick(View v) {

                    switch (v.getId()) {
                        case R.id.imgBtnItmCompany:

                            PopupMenu popup = new PopupMenu(mContext, v);
                            popup.getMenuInflater().inflate(R.menu.popup_menu_act_companies_list,
                                    popup.getMenu());
                            popup.show();
                            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                /**
                                 *
                                 * @param item
                                 * @return
                                 */
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {
                                    switch (item.getItemId()) {

                                        case R.id.ItmCompanyEdit:

                                            Bundle dataBundle = new Bundle();
                                            dataBundle.putString("id", currentCompany.getDbId());
                                            Intent intent = new Intent(mContext, ActCompany.class);
                                            intent.putExtras(dataBundle);
                                            mContext.startActivity(intent);

                                            return true;
                                        case R.id.ItmCompanyAddProject:
                                            String companyID = currentCompany.getDbId();
                                            addProject(companyID);
                                            break;
                                        case R.id.ItmCompanyRemoveProject:
                                            String currentCompanyDbId = currentCompany.getDbId();
                                            removeProject(currentCompanyDbId);
                                            break;

                                        default:
                                            break;
                                    }

                                    return true;
                                }
                            });

                            break;

                        default:
                            break;
                    }

                }
            });

        } catch (Exception e) {

            e.printStackTrace();
        }
        return resultView;
    }

    /**
     *
     * @param aCompanyID
     */
    private void addProject (final String aCompanyID){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.dialog_add_project_to_company, null);
        dialogBuilder.setView(dialogView);

        final TextView mtxtDialogTitle = (TextView) dialogView.findViewById(R.id.txtDialogTitle);
        final EditText metDialogProjectID = (EditText) dialogView.findViewById(R.id.etDialogProjectID);

        dialogBuilder.setTitle("Custom dialog");
        dialogBuilder.setMessage("Enter text below");
        dialogBuilder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                CompanyProjects companyProjects = new CompanyProjects();

                String projectID = metDialogProjectID.getText().toString();
                if (!projectID.isEmpty()) {

                    int companyID = Integer.parseInt(aCompanyID);
                    int ProjectID = Integer.parseInt(projectID);
                    companyProjects.setCompanyId(companyID);
                    companyProjects.setProjectId(ProjectID);


                    AppWorkerPresence.APP_INSTANCE.getWMDBAPI().addProjectToCompany(companyProjects);
                }
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            /**
             *
             * @param dialog
             * @param whichButton
             */
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();

    }

    /**
     *
     * @param aCompanyID
     */
    private void removeProject(final String aCompanyID){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.dialog_add_project_to_company, null);
        dialogBuilder.setView(dialogView);

        final TextView mtxtDialogTitle = (TextView) dialogView.findViewById(R.id.txtDialogTitle);
        final EditText metDialogProjectID = (EditText) dialogView.findViewById(R.id.etDialogProjectID);

        dialogBuilder.setTitle("Custom dialog");
        dialogBuilder.setMessage("Enter text below");
        dialogBuilder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            /**
             *
             * @param dialog
             * @param whichButton
             */
            public void onClick(DialogInterface dialog, int whichButton) {

                CompanyProjects companyProjects = new CompanyProjects();

                String projectID = metDialogProjectID.getText().toString();
                if (!projectID.isEmpty()) {

                    int companyID = Integer.parseInt(aCompanyID);
                    int ProjectID = Integer.parseInt(projectID);
                    companyProjects.setCompanyId(companyID);
                    companyProjects.setProjectId(ProjectID);


                    AppWorkerPresence.APP_INSTANCE.getWMDBAPI().removeProjectFromCompany(ProjectID , companyID);
                }
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            /**
             *
             * @param dialog
             * @param whichButton
             */
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    /**
     *
     * @param aCompanyID
     */
    private void removeWorker(final String aCompanyID) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.act_dialog, null);
        dialogBuilder.setView(dialogView);

        final TextView mtxtDialogTitle = (TextView) dialogView.findViewById(R.id.txtDialogTitle);
        //final EditText metDialogProjectID = (EditText) dialogView.findViewById(R.id.etDialogProjectID);
        final EditText metDialogWorkerID = (EditText) dialogView.findViewById(R.id.etDialogWorkerID);
        final EditText metDialogSalary = (EditText) dialogView.findViewById(R.id.etDialogSalary);


        dialogBuilder.setTitle("Custom dialog");
        dialogBuilder.setMessage("Enter text below");
        dialogBuilder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            /**
             *
             * @param dialog
             * @param whichButton
             */
            public void onClick(DialogInterface dialog, int whichButton) {

                CompanyWorkers companyWorkers = new CompanyWorkers();

                String workerPhone = metDialogWorkerID.getText().toString();
                String salary = metDialogSalary.getText().toString();
                if (!workerPhone.isEmpty())
                {
                    String sWorkerId = AppWorkerPresence.APP_INSTANCE.getWMDBAPI().loadWorkerID(workerPhone);

                    int companyID = Integer.parseInt(aCompanyID);
                    int workerID = Integer.parseInt(sWorkerId);
                    int Salary = Integer.parseInt(salary);

                    companyWorkers.setCompanyId(companyID);
                    companyWorkers.setWorkerId(workerID);
                    companyWorkers.setStatus(1);
                    companyWorkers.setSalary(Salary);

                    AppWorkerPresence.APP_INSTANCE.getWMDBAPI().removeWorkerFromCompany(workerID, companyID);
                }
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            /**
             *
             * @param dialog
             * @param whichButton
             */
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();

    }

    /**
     *
     * @param aCompanyID
     */
    private void addManager(final String aCompanyID) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.act_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText metDialogWorkerID = (EditText) dialogView.findViewById(R.id.etDialogWorkerID);
        final EditText metDialogSalary = (EditText) dialogView.findViewById(R.id.etDialogSalary);


        dialogBuilder.setTitle("Custom dialog");
        dialogBuilder.setMessage("Enter text below");
        dialogBuilder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            /**
             *
             * @param dialog
             * @param whichButton
             */
            public void onClick(DialogInterface dialog, int whichButton) {

                CompanyWorkers companyWorkers = new CompanyWorkers();

                String workerPhone = metDialogWorkerID.getText().toString();
                String salary = metDialogSalary.getText().toString();
                if (!workerPhone.isEmpty())
                {
                    String sWorkerId = AppWorkerPresence.APP_INSTANCE.getWMDBAPI().loadWorkerID(workerPhone);

                    int companyID = Integer.parseInt(aCompanyID);
                    int workerID = Integer.parseInt(sWorkerId);
                    int Salary = Integer.parseInt(salary);

                    companyWorkers.setCompanyId(companyID);
                    companyWorkers.setWorkerId(workerID);
                    companyWorkers.setStatus(2);
                    companyWorkers.setSalary(Salary);

                    AppWorkerPresence.APP_INSTANCE.getWMDBAPI().addWorkerToCompany(companyWorkers);
                }
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            /**
             *
             * @param dialog
             * @param whichButton
             */
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();

    }

    /**
     *
     * @param aCompanyID
     */
    private void removeManager(final String aCompanyID) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.act_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText metDialogWorkerID = (EditText) dialogView.findViewById(R.id.etDialogWorkerID);
        final EditText metDialogSalary = (EditText) dialogView.findViewById(R.id.etDialogSalary);


        dialogBuilder.setTitle("Custom dialog");
        dialogBuilder.setMessage("Enter text below");
        dialogBuilder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            /**
             *
             * @param dialog
             * @param whichButton
             */
            public void onClick(DialogInterface dialog, int whichButton) {

                CompanyWorkers companyWorkers = new CompanyWorkers();

                String workerPhone = metDialogWorkerID.getText().toString();
                String salary = metDialogSalary.getText().toString();
                if (!workerPhone.isEmpty())
                {
                    String sWorkerId = AppWorkerPresence.APP_INSTANCE.getWMDBAPI().loadWorkerID(workerPhone);

                    int companyID = Integer.parseInt(aCompanyID);
                    int workerID = Integer.parseInt(sWorkerId);
                    int Salary = Integer.parseInt(salary);

                    companyWorkers.setCompanyId(companyID);
                    companyWorkers.setWorkerId(workerID);
                    companyWorkers.setStatus(2);
                    companyWorkers.setSalary(Salary);

                    AppWorkerPresence.APP_INSTANCE.getWMDBAPI().removeWorkerFromCompany(workerID, companyID);
                }
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            /**
             *
             * @param dialog
             * @param whichButton
             */
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();

    }
}