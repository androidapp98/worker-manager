package il.co.techschool.workmanageapp.activities.entities;

import android.content.Context;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.Date;

/**
 * Created by Admin on 11/26/2017.
 */
public class Project {
    @SerializedName("db_id")
    @Expose
    private Integer dbId;
    @SerializedName("project_name")
    @Expose
    private String projectName;
    @SerializedName("manager_id")
    @Expose
    private Integer managerId;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("start_date")
    @Expose
    private Date startDate;
    @SerializedName("end_date")
    @Expose
    private Date endDate;

    private Context mContext;

    /**
     *
     */
    public Project() {
    }

    /**
     *
     * @param aDbId
     * @param aProjectName
     * @param aManagerId
     * @param aLocation
     * @param aDescription
     * @param aStatus
     * @param aStartDate
     * @param aEndDate
     */
    public Project(Integer aDbId, String aProjectName, Integer aManagerId, String aLocation
            , String aDescription, Integer aStatus, Date aStartDate, Date aEndDate) {
        dbId = aDbId;
        projectName = aProjectName;
        managerId = aManagerId;
        location = aLocation;
        description = aDescription;
        status = aStatus;
        startDate = aStartDate;
        endDate = aEndDate;
    }

    public Integer getDbId() {
        return dbId;
    }

    /**
     *
     * @param aDbId
     */
    public void setDbId(Integer aDbId) {
        dbId = aDbId;
    }

    /**
     *
     * @return
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     *
     * @param aProjectName
     */
    public void setProjectName(String aProjectName) {
        projectName = aProjectName;
    }

    /**
     *
     * @return
     */
    public Integer getManagerId() {
        return managerId;
    }

    /**
     *
     * @param aManagerId
     */
    public void setManagerId(Integer aManagerId) {
        managerId = aManagerId;
    }

    /**
     *
     * @return
     */
    public String getLocation() {
        return location;
    }

    /**
     *
     * @param aLocation
     */
    public void setLocationId(String aLocation) {
        location = aLocation;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param aDescription
     */
    public void setDescription(String aDescription) {
        description = aDescription;
    }

    /**
     *
     * @return
     */
    public Integer getProjectStatus() {
        return status;
    }

    /**
     *
     * @param aStatus
     */
    public void setProjectStatus(Integer aStatus) {
        status = aStatus;
    }

    /**
     *
     * @return
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     *
     * @param aStartDate
     */
    public void setStartDate(Date aStartDate) {
        startDate = aStartDate;
    }

    /**
     *
     * @return
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     *
     * @param aEndDate
     */
    public void setEndDate(Date aEndDate) {
        endDate = aEndDate;
    }

    /**
     *
     * @param aProject
     */
    public void updateFrom(Project aProject) {
        dbId = aProject.dbId;
        projectName = aProject.projectName;
        managerId = aProject.managerId;
        location = aProject.location;
        description = aProject.description;
        status = aProject.status;
        startDate = aProject.startDate;
        endDate = aProject.endDate;
    }
}