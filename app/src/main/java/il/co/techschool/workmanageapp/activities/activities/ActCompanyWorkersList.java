package il.co.techschool.workmanageapp.activities.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import il.co.techschool.workmanageapp.R;
import il.co.techschool.workmanageapp.activities.adapters.AdapterCompanyWorkers;
import il.co.techschool.workmanageapp.activities.common.AppWorkerPresence;


/**
 * Created by Admin on 1/2/2018.
 */

public class ActCompanyWorkersList extends AppCompatActivity {
    private ListView mlstWrkListListView;

    private Context mContext;
    private AppWorkerPresence mAppWorkerPresence;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_company_workers_list);

        mAppWorkerPresence = (AppWorkerPresence) getApplication();
        mContext = this;

        initComponents();
    }

    /**
     *
     */
    @Override
    protected void onResume() {
        super.onResume();

        AdapterCompanyWorkers adapterCompanyWorkers;
        // start using the adapter after you have the data loading...
        adapterCompanyWorkers = new AdapterCompanyWorkers(mContext, AppWorkerPresence.APP_INSTANCE.getArrayListCompanyWorkers());
        mlstWrkListListView.setAdapter(adapterCompanyWorkers);
    }

    /**
     *
     */
    private void initComponents() {
        mlstWrkListListView = (ListView) findViewById(R.id.lstProjectWorkers);
    }
}
