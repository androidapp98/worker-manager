package il.co.techschool.workmanageapp.activities.arrays;

import java.util.ArrayList;
import java.util.Date;

import il.co.techschool.workmanageapp.activities.entities.Company;


/**
 * Created by Admin on 1/14/2018.
 */

public class ArrayListWorkerCompanies extends ArrayList<Company> {

    private Date loaded;

    /**
     *
     * @return
     */
    public Date getLoaded() {
        return loaded;
    }

    /**
     *
     * @param loaded
     */
    public void setLoaded(Date loaded) {
        this.loaded = loaded;
    }

    /**
     *
     * @param aUid
     * @return
     */
    public Company findCompanyById(String aUid) {
        Company companyFound;
        int iIndex;
        int iSize;

        companyFound = null;

        iSize = size();

        for (iIndex = 0; iIndex < iSize; iIndex++) {

            if (aUid.compareToIgnoreCase(get(iIndex).getDbId()) == 0) {
                companyFound = get(iIndex);
                break;
            }
        }

        return companyFound;
    }

}
