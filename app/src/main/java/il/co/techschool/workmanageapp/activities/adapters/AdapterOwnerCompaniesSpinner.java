package il.co.techschool.workmanageapp.activities.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import il.co.techschool.workmanageapp.R;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListOwnerCompanies;
import il.co.techschool.workmanageapp.activities.common.AppWorkerPresence;
import il.co.techschool.workmanageapp.activities.entities.Company;


/**
 * Created by Admin on 1/24/2018.
 */

public class AdapterOwnerCompaniesSpinner extends BaseAdapter
{
    private Context mContext;
    private ArrayListOwnerCompanies mArrayListOwnerCompanies;

    /**
     *
     * @param aContext
     * @param aArrayListOwnerCompanies
     */
    public AdapterOwnerCompaniesSpinner(Context aContext, ArrayListOwnerCompanies aArrayListOwnerCompanies) {
        mArrayListOwnerCompanies = aArrayListOwnerCompanies;
        mContext = aContext;

    }

    /**
     *
     * @return
     */
    @Override
    public int getCount() {
        return mArrayListOwnerCompanies.size();
    }

    /**
     *
     * @param i
     * @return
     */
    @Override
    public Object getItem(int i) {
        return mArrayListOwnerCompanies.getLoaded();
    }

    /**
     *
     * @param i
     * @return
     */
    @Override
    public long getItemId(int i) {
        return 0;
    }

    /**
     *
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View resultView;
        LayoutInflater layoutInflater;
        final Company currentCompany;

        TextView txtCompanyName;

        if (convertView == null)
        {
            layoutInflater = (LayoutInflater) AppWorkerPresence.APP_INSTANCE.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            // create the custom layout to show the "companies"
            resultView = layoutInflater.inflate(R.layout.lo_item_company, null);

            // find the objects inside the custom layout...
            txtCompanyName = (TextView) resultView.findViewById((R.id.txtItmCompanyName));
            // add all the "objects" to the "tag" list
            //resultView.setTag( R.id.<item it>, itemObject);

            resultView.setTag(R.id.txtItmCompanyName, txtCompanyName);

        } else
        {
            resultView = convertView;

            // extract all the pointers to the objects from the "tag" list.
            txtCompanyName = (TextView) resultView.getTag(R.id.txtItmCompanyName);
        }
        currentCompany = mArrayListOwnerCompanies.get(position);

        // update custom items in the view with the data from the "currentWorker";
        txtCompanyName.setText(currentCompany.getCompanyName());
        txtCompanyName.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param v
             */
            @Override
            public void onClick(View v)
            {
                //Toast.makeText(mContext, " get click", Toast.LENGTH_SHORT).show();
                AppWorkerPresence.APP_INSTANCE.setProjectID("");
                String companyID = currentCompany.getDbId();
                AppWorkerPresence.APP_INSTANCE.setCompanyID(companyID);
            }
        });
        return resultView;
    }
}
