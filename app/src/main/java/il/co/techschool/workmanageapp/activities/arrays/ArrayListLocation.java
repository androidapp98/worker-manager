package il.co.techschool.workmanageapp.activities.arrays;


import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Date;

import il.co.techschool.workmanageapp.activities.entities.WorkerLocation;
import il.co.techschool.workmanageapp.activities.events.EventArrayListWorkerLocationsChange;


/**
 * Created by Admin on 11/24/2017.
 */

public class ArrayListLocation extends ArrayList<WorkerLocation> {
    private Date loaded;

    /**
     *
     * @return
     */
    public Date getLoaded() {
        return loaded;
    }

    /**
     *
     * @param loaded
     */
    public void setLoaded(Date loaded) {
        this.loaded = loaded;
    }

    /**
     *
     * @param aUid
     * @return
     */
    public WorkerLocation findWorkerLocationById(String aUid) {
        WorkerLocation workerLocationFound;
        int iIndex;
        int iSize;

        workerLocationFound = null;

        iSize = size();

        for (iIndex = 0; iIndex < iSize; iIndex++) {


            if (aUid.compareToIgnoreCase(get(iIndex).getDbId()) == 0) {
                workerLocationFound = get(iIndex);
                break;
            }
        }

        return workerLocationFound;
    }

    /**
     *
     * @param aWorkerLocation
     * @return
     */
    public WorkerLocation updateWorkerLocation(WorkerLocation aWorkerLocation) {
        WorkerLocation currentWorkerLocation;

        currentWorkerLocation = findWorkerLocationById(aWorkerLocation.getDbId());

        if (currentWorkerLocation != null) {
            currentWorkerLocation.updateFrom(aWorkerLocation);
            EventBus.getDefault().post(new EventArrayListWorkerLocationsChange());
            return currentWorkerLocation;
        } else {
            return null;
        }
    }

    /**
     *
     * @param aWorkerLocation
     */
    public void removeWorkerLocation(WorkerLocation aWorkerLocation) {
        WorkerLocation currentWorkerLocation;

        currentWorkerLocation = findWorkerLocationById(aWorkerLocation.getDbId());

        if (currentWorkerLocation != null) {
            remove(currentWorkerLocation);
            EventBus.getDefault().post(new EventArrayListWorkerLocationsChange());
        }
    }

    /**
     *
     * @param aWorkerLocation
     * @return
     */
    @Override
    public boolean add(WorkerLocation aWorkerLocation) {
        boolean result;
        result = super.add(aWorkerLocation);
        EventBus.getDefault().post(new EventArrayListWorkerLocationsChange());
        return result;
    }
}
