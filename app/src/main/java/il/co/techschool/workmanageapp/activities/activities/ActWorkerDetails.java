package il.co.techschool.workmanageapp.activities.activities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

import il.co.techschool.workmanageapp.R;
import il.co.techschool.workmanageapp.activities.common.AppWorkerPresence;
import il.co.techschool.workmanageapp.activities.common.Utils;
import il.co.techschool.workmanageapp.activities.entities.Worker;

import static il.co.techschool.workmanageapp.activities.activities.ActLogin.PhoneNumber;


/**
 * Created by Admin on 10/14/2017.
 */

public class ActWorkerDetails extends AppCompatActivity {

    private static final String TAG = "ActWorkerDetails";
    private Context mContext;

    private EditText metWrkDetailsFirstName;
    private EditText metWrkDetailsLastName;
    private EditText metWrkDetailsIsraeliID;

    private TextView metWrkDetailsDOB;
    private DatePickerDialog mDatePickerDialog;

    private Spinner mspEduLevel;
    private TextView mtxtEduLevel;
    private EditText metWrkDetailsWorkKind;

    private Button mbtnWrkDetailsSaveProperties;

    public Bundle getBundle = null;

    private Worker mWorker;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_worker_details);

        mContext = this;

        initComponents();
        iniitComponentsListener();
    }

    /**
     *
     */
    @Override
    protected void onResume() {
        super.onResume();
        // check if we got id to load the user details
        getBundle = this.getIntent().getExtras();
        if (getBundle != null) {
            if (getBundle.containsKey("ID")) {
                String id = getBundle.getString("ID");

                if (id != null) {
                    loadWorkerDetails(id);
                }
            }
        }
    }

    /**
     * initialize components
     */
    private void initComponents() {

        metWrkDetailsFirstName = (EditText) findViewById(R.id.etWrkDetailsFirstName);
        metWrkDetailsLastName = (EditText) findViewById(R.id.etWrkDetailsLastName);
        metWrkDetailsIsraeliID = (EditText) findViewById(R.id.etWrkDetailsIsraeliID);
        metWrkDetailsDOB = (TextView) findViewById(R.id.etWrkDetailsDOB);
        mspEduLevel = (Spinner) findViewById(R.id.spEduLevel);
        mtxtEduLevel = (TextView) findViewById(R.id.txtWrkDetailsEducationLevel);
        metWrkDetailsWorkKind = (EditText) findViewById(R.id.etWrkDetailsWorkKind);
        mbtnWrkDetailsSaveProperties = (Button) findViewById(R.id.btnWrkDetailsSaveProperties);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.Education_Level, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        mspEduLevel.setAdapter(adapter);

    }

    /**
     *adding onClick listener
     */
    private void iniitComponentsListener() {
        mbtnWrkDetailsSaveProperties.setOnClickListener(OnClick);
        metWrkDetailsDOB.setOnClickListener(OnClick);
        mspEduLevel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = parent.getItemAtPosition(position).toString();
                Toast.makeText(mContext, selected, Toast.LENGTH_SHORT).show();
                mtxtEduLevel.setText(selected);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    View.OnClickListener OnClick = new View.OnClickListener() {
        /**
         *
         * @param view
         */
        @Override
        public void onClick(View view) {

            switch (view.getId()) {
                case R.id.etWrkDetailsDOB:
                    // calender class's instance and get current date , month and year from calender
                    final Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR); // current year
                    int mMonth = c.get(Calendar.MONTH) + 1; // current month the +1 bc zero based
                    int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                    // date picker dialog
                    mDatePickerDialog = new DatePickerDialog(ActWorkerDetails.this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    // set day of month , month and year value in the edit text
                                    metWrkDetailsDOB.setText(dayOfMonth + "/"
                                            + (monthOfYear) + "/" + year);

                                }
                            }, mYear, mMonth, mDay);
                    mDatePickerDialog.show();

                    break;
                case R.id.btnWrkDetailsSaveProperties:
                    // check if it is new worker or not
                    if (mWorker == null) {
                        mWorker = new Worker();
                        mspEduLevel.setVisibility(View.VISIBLE);
                    }
                    // get written details
                    String errorMessage = "";
                    Toast errorToast;

                    String firstName = metWrkDetailsFirstName.getText().toString();
                    String lastName = metWrkDetailsLastName.getText().toString();
                    String israeliID = metWrkDetailsIsraeliID.getText().toString();
                    String tmp = metWrkDetailsDOB.getText().toString();
                    String eduLevel = mtxtEduLevel.getText().toString();
                    String workKind = metWrkDetailsWorkKind.getText().toString();


                    if (firstName.isEmpty()){
                        errorMessage = "First name";
                    } else {
                        if (lastName.isEmpty()){
                            errorMessage = "Last name";
                        } else {
                            if (tmp.isEmpty()){
                                errorMessage = "Date of birth";
                            } else {
                                    if (workKind.isEmpty()){
                                        errorMessage = "Work kind";
                                    }
                                }
                            }
                        }


//                    if (firstName.isEmpty() || lastName.isEmpty() || lastName.isEmpty() || tmp.isEmpty() || educationLevel.isEmpty() || workKind.isEmpty()) {
                    if (!errorMessage.isEmpty()){
                        // check if first name field is empty
                        errorToast = Toast.makeText(ActWorkerDetails.this, errorMessage + " field is empty! ", Toast.LENGTH_SHORT);
                        errorToast.show();
                    } else {
                        // check if israeliID number is correct
                        int numberId = Integer.parseInt(israeliID);
                        if (!CheckId(numberId)) {
                            errorToast = Toast.makeText(ActWorkerDetails.this, " IsraeliID is not correct! ", Toast.LENGTH_SHORT);
                            errorToast.show();
                        }
                        int existingWorkerID;
                        //     get id from db if exist
                        existingWorkerID = AppWorkerPresence.APP_INSTANCE.getWMDBAPI().loadWorkerIfIsraeliIDExist(israeliID);
                        //     check if israeliID is Exist
                        if (existingWorkerID != -1) {
                            errorToast = Toast.makeText(ActWorkerDetails.this, " IsraeliID is Exist! ", Toast.LENGTH_SHORT);
                            errorToast.show();
                        }
                        // compare string to date
                        Date dateOfBirth = Utils.StringToDate(tmp);
                        // get user phone number
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("mypref", 0); // 0 - for private mode
                        String phoneNumber = pref.getString(PhoneNumber, ""); // getting String

                        // save details
                        mWorker.setFirstName(firstName);
                        mWorker.setLastName(lastName);
                        mWorker.setIsraeliID(israeliID);
                        mWorker.setDob(dateOfBirth);
                        mWorker.setPhoneNumber(phoneNumber);
                        mWorker.setEducationLevel(eduLevel);
                        mWorker.setWorkKind(workKind);

                        AppWorkerPresence.APP_INSTANCE.getWMDBAPI().saveWorker(mWorker);

                        // show message mWorker saved
                        errorToast = Toast.makeText(ActWorkerDetails.this, " Worker saved successfully! ", Toast.LENGTH_SHORT);
                        errorToast.show();

                        Intent intent = new Intent(ActWorkerDetails.this, ActWorkTime.class);
                        startActivity(intent);
                    }
            }
        }
    };

    /**
     * load worker details
     * @param aWorkerID
     */
    public void loadWorkerDetails(String aWorkerID) {

        mWorker = AppWorkerPresence.APP_INSTANCE.getArrayListWorkers().findWorkerById(aWorkerID);
        metWrkDetailsFirstName.setText(mWorker.getFirstName());
        metWrkDetailsLastName.setText(mWorker.getLastName());
        metWrkDetailsIsraeliID.setText(mWorker.getIsraeliID());
        String dateOfBirth = Utils.DateToString(mWorker.getDob());
        metWrkDetailsDOB.setText(dateOfBirth);
        mtxtEduLevel.setText(mWorker.getEducationLevel());
        metWrkDetailsWorkKind.setText(mWorker.getWorkKind());
        mbtnWrkDetailsSaveProperties.setVisibility(View.INVISIBLE);
    }

    /**
     * check number of digits
     * @param Number
     * @return
     */
    public static int SumDigits(int Number) {
        int sum = 0;
        while (Number != 0) {
            int temp1 = Number % 10;
            sum += temp1;
            Number = Number / 10;
        }
        return sum;
    }

    /**
     * check israel id by logic
     * @param number
     * @return
     */
    public static boolean CheckId(int number) {
        int LastNumber = number % 10;
        number = number / 10;
        int sum = 0;
        for (int i = 8; i >= 1; i--) {
            int calculate = number % 10;
            if (i % 2 == 1) {
                sum += calculate;
            } else {
                calculate = calculate * 2;
                int SumDigits = SumDigits(calculate);
                sum += SumDigits;
            }
            number = number / 10;
        }
        int res;
        if (sum % 10 == 0) {
            res = 0;
        } else {
            res = 10 - (Math.abs((sum % 10)));
        }

        if (LastNumber == res) {
            return true;
        } else {
            return false;
        }
    }

}