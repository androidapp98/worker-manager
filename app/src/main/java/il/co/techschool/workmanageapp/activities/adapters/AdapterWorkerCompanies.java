package il.co.techschool.workmanageapp.activities.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import il.co.techschool.workmanageapp.R;
import il.co.techschool.workmanageapp.activities.activities.ActCompany;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListWorkerCompanies;
import il.co.techschool.workmanageapp.activities.common.AppWorkerPresence;
import il.co.techschool.workmanageapp.activities.entities.Company;

/**
 * Created by Admin on 1/15/2018.
 */

public class AdapterWorkerCompanies extends BaseAdapter {

    private Context mContext;
    private ArrayListWorkerCompanies mArrayListWorkerCompanies;

    /**
     *
     * @param aContext
     * @param aArrayListWorkerCompanies
     */
    public AdapterWorkerCompanies(Context aContext, ArrayListWorkerCompanies aArrayListWorkerCompanies) {
        mArrayListWorkerCompanies = aArrayListWorkerCompanies;
        mContext = aContext;
    }

    /**
     *
     * @return
     */
    @Override
    public int getCount() {
        return mArrayListWorkerCompanies.size();
    }

    /**
     *
     * @param i
     * @return
     */
    @Override
    public Object getItem(int i) {
        return mArrayListWorkerCompanies.getLoaded();
    }

    /**
     *
     * @param i
     * @return
     */
    @Override
    public long getItemId(int i) {
        return 0;
    }

    /**
     *
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View resultView;
        LayoutInflater layoutInflater;
        final Company currentCompany;

        ImageView imgCompanyIcon;
        TextView txtCompanyName;
        ImageButton imgBtnCompany;

        if (convertView == null) {
            layoutInflater = (LayoutInflater) AppWorkerPresence.APP_INSTANCE.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            // create the custom layout to show the "companies"
            resultView = layoutInflater.inflate(R.layout.lo_item_company, null);

            // find the objects inside the custom layout...
            imgCompanyIcon = (ImageView) resultView.findViewById((R.id.imgItmCompanyIcon));
            txtCompanyName = (TextView) resultView.findViewById((R.id.txtItmCompanyName));
            imgBtnCompany = (ImageButton) resultView.findViewById((R.id.imgBtnItmCompany));

            // add all the "objects" to the "tag" list
            //resultView.setTag( R.id.<item it>, itemObject);
            resultView.setTag(R.id.imgItmCompanyIcon, imgCompanyIcon);
            resultView.setTag(R.id.txtItmCompanyName, txtCompanyName);
            resultView.setTag(R.id.imgBtnItmCompany, imgBtnCompany);

        } else {
            resultView = convertView;

            // extract all the pointers to the objects from the "tag" list.
            imgCompanyIcon = (ImageView) resultView.getTag(R.id.imgItmCompanyIcon);
            txtCompanyName = (TextView) resultView.getTag(R.id.txtItmCompanyName);
            imgBtnCompany = (ImageButton) resultView.getTag(R.id.imgBtnItmCompany);
        }
        currentCompany = mArrayListWorkerCompanies.get(position);

        // update custom items in the view with the data from the "currentWorker";
        txtCompanyName.setText(currentCompany.getCompanyName());
        byte[] icon = currentCompany.getCompanyIcon();
        if (icon != null) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(icon, 0, icon.length);
            imgCompanyIcon.setImageBitmap(bitmap);
        }

        txtCompanyName.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param v
             */
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext, " get click", Toast.LENGTH_SHORT).show();

                Bundle dataBundle = new Bundle();
                dataBundle.putString("id", currentCompany.getDbId());
                Intent intent = new Intent(mContext, ActCompany.class);
                intent.putExtras(dataBundle);
                mContext.startActivity(intent);

            }
        });

        return resultView;
    }
}
