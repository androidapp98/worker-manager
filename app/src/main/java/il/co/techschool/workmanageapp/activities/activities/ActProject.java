package il.co.techschool.workmanageapp.activities.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import il.co.techschool.workmanageapp.R;
import il.co.techschool.workmanageapp.activities.common.AppWorkerPresence;
import il.co.techschool.workmanageapp.activities.common.Utils;
import il.co.techschool.workmanageapp.activities.entities.Project;

import static il.co.techschool.workmanageapp.activities.activities.ActLogin.PhoneNumber;


/**
 * Created by Admin on 11/25/2017.
 */

public class ActProject extends Activity {

    private TextView mtxtNewProjectTitle;
    private static final String TAG = "ActProject";


    private EditText metNewProjectProjectName;
    private EditText metNewProjectLocation;
    private EditText metNewProjectDescription;
    private EditText metNewProjectStatus;

    private TextView metNewProjectStartDate;
    private TextView metNewProjectEndDate;
    private DatePickerDialog.OnDateSetListener mDateSetListener;


    private Button mbtnProjectWorkersList;
    private Button mbtnNewProjectSave;
    private Button mbtnProjectIcon;

    public Bundle getBundle = null;

    private Project mProject;

    private Context mContext;

    private static final int PICK_IMAGE = 100;
    final int REQUEST_CODE_GALLERY = 999;

    Uri mImgUri;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_project);

        mContext = this;

        initializeComponents();
        initializeComponentsListeners();
    }

    /**
     *
     */
    @Override
    protected void onResume() {
        super.onResume();
        // check if there is a project to download its details or start new one
        getBundle = this.getIntent().getExtras();
        if (getBundle != null) {
            if (getBundle.containsKey("id")) {
                loadProjectDetails(getBundle.getInt("id"));

            }
        }
    }

    /**
     *
     */
    void initializeComponents() {
        mtxtNewProjectTitle = (TextView) findViewById(R.id.txtNewProjectTitle);
        metNewProjectProjectName = (EditText) findViewById(R.id.etNewProjectProjectName);
        metNewProjectLocation = (EditText) findViewById(R.id.etNewProjectLocation);
        metNewProjectDescription = (EditText) findViewById(R.id.etNewProjectDescription);
        metNewProjectStatus = (EditText) findViewById(R.id.etNewProjectStatus);
        metNewProjectStartDate = (TextView) findViewById(R.id.etNewProjectStartDate);
        metNewProjectEndDate = (TextView) findViewById(R.id.etNewProjectEndDate);

        mbtnProjectWorkersList = (Button) findViewById(R.id.btnProjectWorkersList);
        mbtnNewProjectSave = (Button) findViewById(R.id.btnNewProjectSave);


    }

    /**
     *
     */
    void initializeComponentsListeners() {
        mbtnProjectWorkersList.setOnClickListener(OnClick);
        mbtnNewProjectSave.setOnClickListener(OnClick);

        metNewProjectStartDate.setOnClickListener(OnClick);
        metNewProjectEndDate.setOnClickListener(OnClick);
    }

    View.OnClickListener OnClick = new View.OnClickListener() {

        /**
         *
         * @param v
         */
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.etNewProjectStartDate:

                    Calendar cal = Calendar.getInstance();
                    int year = cal.get(Calendar.YEAR);
                    int month = cal.get(Calendar.MONTH);
                    int day = cal.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog dialog = new DatePickerDialog(
                            ActProject.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                            mDateSetListener,
                            year, month, day);
                    Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.show();

                    mDateSetListener = new DatePickerDialog.OnDateSetListener() {
                        /**
                         *
                         * @param view
                         * @param year
                         * @param month
                         * @param day
                         */
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int day)
                        {
                            month = month + 1;
                            Log.d(TAG, "onDateSet: mm/dd/yyyy: "  + day + "/" + month + "/" + year);
                            String date = day + "/" + month + "/" + year;
                            metNewProjectStartDate.setText(date);
                        }
                    };

                    break;

                case R.id.etNewProjectEndDate:

                    Calendar cal2 = Calendar.getInstance();
                    int year2 = cal2.get(Calendar.YEAR);
                    int month2 = cal2.get(Calendar.MONTH);
                    int day2 = cal2.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog dialog2 = new DatePickerDialog(
                            ActProject.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                            mDateSetListener,
                            year2, month2, day2);
                    Objects.requireNonNull(dialog2.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog2.show();

                    mDateSetListener = new DatePickerDialog.OnDateSetListener() {
                        /**
                         *
                         * @param view
                         * @param year
                         * @param month
                         * @param day
                         */
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int day)
                        {
                            month = month + 1;
                            Log.d(TAG, "onDateSet: mm/dd/yyyy: "  + day + "/" + month + "/" + year);
                            String date = day + "/" + month + "/" + year;
                            metNewProjectEndDate.setText(date);
                        }
                    };

                    break;

                case R.id.btnNewProjectSave:

                    // save project details
                    saveProjectDetails();

                    break;
                case R.id.btnProjectWorkersList:

                    if (mProject != null) {
                        // get projectID
                        String ProjectID = String.valueOf(mProject.getDbId());
                        // set projectID
                        AppWorkerPresence.APP_INSTANCE.setProjectID(ProjectID);
                        // load project workers in a list
                     //   Intent ActWorkersList = new Intent(ActProject.this, ActProjectWorkersList.class);
                  //      startActivity(ActWorkersList);
                        Bundle dataBundle = new Bundle();
                        dataBundle.putInt("id", mProject.getDbId());
                        Intent intent = new Intent(mContext, ActProjectWorkersList.class);
                        intent.putExtras(dataBundle);
                        mContext.startActivity(intent);
                    } else {
                        // show message
                        Toast errorToast = Toast.makeText(ActProject.this, " There is no workers for this project yet ! ", Toast.LENGTH_SHORT);
                        errorToast.show();
                    }

                    break;

            }
        }
    };

    /**
     *
     * @param image
     * @return
     */
    public static byte[] imageViewToByte(ImageView image) {
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    /**
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == REQUEST_CODE_GALLERY){
            if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Intent intent = new Intent(Intent.ACTION_PICK ,
                MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                intent.setType("image/*");
                // try cut image
//                intent.putExtra("crop", "true");
//                intent.putExtra("aspectX", 1);
//                intent.putExtra("aspectY", 1);
//                intent.putExtra("outputX", 200);
//                intent.putExtra("outputY", 200);
//                intent.putExtra("return-data", true);
                startActivityForResult(intent, REQUEST_CODE_GALLERY);
            }
            else {
                Toast.makeText(getApplicationContext(), "You don't have permission to access file location!", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK && data != null){
            mImgUri = data.getData();
            try {
                InputStream inputStream = getContentResolver().openInputStream(mImgUri);

                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                //mimgProjectIcon.setImageURI(mImgUri);
                //mimgProjectIcon.setImageBitmap((Bitmap) data.getExtras().get("data"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /*
        save and update project
         */

    /**
     *
     */
    public void saveProjectDetails() {
        // check if id != null as update project and if not create a new project
        if (mProject == null) {
            mProject = new Project();
        }

        // get written details
        String projectName = metNewProjectProjectName.getText().toString();
      //  String tmpManagerPhone = metNewProjectManagerID.getText().toString();
        String location = metNewProjectLocation.getText().toString();
        String description = metNewProjectDescription.getText().toString();
        String tmpStatus = metNewProjectStatus.getText().toString();
        String tmpstartdate = metNewProjectStartDate.getText().toString();
        String tmpendDate = metNewProjectEndDate.getText().toString();

        // || tmpManagerPhone.isEmpty()
        if (projectName.isEmpty()  || location.isEmpty() || description.isEmpty() ||
                tmpStatus.isEmpty() || tmpstartdate.isEmpty()) {
            // show message
            Toast errorToast = Toast.makeText(ActProject.this, " Fields are empty! ", Toast.LENGTH_SHORT);
            errorToast.show();

        } else {

            // get user phone number
            SharedPreferences pref = getApplicationContext().getSharedPreferences("mypref", 0); // 0 - for private mode
            String phoneNumber = pref.getString(PhoneNumber, ""); // getting String
            // get owner id by his phone number
            String tmpManagerID = AppWorkerPresence.APP_INSTANCE.getWMDBAPI().loadWorkerID(phoneNumber);
            if (tmpManagerID == null) {
                // show message
                Toast errorToast = Toast.makeText(ActProject.this, " Manager phone number isn't correct ", Toast.LENGTH_SHORT);
                errorToast.show();
            } else {
                // compare string to integer
                int managerID = Integer.parseInt(tmpManagerID);

                int status = Integer.parseInt(tmpStatus);


                // compare string to date
                Date startDate = Utils.StringToDate(tmpstartdate);



                // save details
                mProject.setProjectName(projectName);
                mProject.setManagerId(managerID);
                mProject.setLocationId(location);
                mProject.setDescription(description);
                mProject.setProjectStatus(status);
                mProject.setStartDate(startDate);
                if (!tmpendDate.isEmpty()) {
                    Date endDate = Utils.StringToDate(tmpendDate);
                    mProject.setEndDate(endDate);
                }


                AppWorkerPresence.APP_INSTANCE.getWMDBAPI().saveProject(mProject);

                // show message mProject saved
                Toast errorToast = Toast.makeText(ActProject.this, " Project saved successfully! ", Toast.LENGTH_SHORT);
                errorToast.show();

                // intent back to  list
                Intent actWorkersList = new Intent(ActProject.this, ActTabsProjects.class);
                startActivity(actWorkersList);
            }
        }
    }
    /*
    load project details
     */

    /**
     *
     * @param aProjectID
     */
    public void loadProjectDetails(Integer aProjectID) {
        mProject = AppWorkerPresence.APP_INSTANCE.getArrayListWorkerProjects().findProjectById(aProjectID);
        if (mProject == null){
            mProject = AppWorkerPresence.APP_INSTANCE.getArrayListManagerProjects().findProjectById(aProjectID);
        }
        metNewProjectProjectName.setText(mProject.getProjectName());
        metNewProjectLocation.setText(mProject.getLocation());
        metNewProjectDescription.setText(mProject.getDescription());
        metNewProjectStatus.setText(String.valueOf(mProject.getProjectStatus()));

        String startDate = Utils.DateToString(mProject.getStartDate());
        String endDate = Utils.DateToString(mProject.getEndDate());

        metNewProjectStartDate.setText(startDate);
        metNewProjectEndDate.setText(endDate);

    }
}
