package il.co.techschool.workmanageapp.activities.arrays;

import java.util.ArrayList;
import java.util.Date;

import il.co.techschool.workmanageapp.activities.entities.Worker;


/**
 * Created by Admin on 12/12/2017.
 */

public class ArrayListProjectWorkers extends ArrayList<Worker> {

    private Date loaded;

    /**
     *
     * @return
     */
    public Date getLoaded() {
        return loaded;
    }

    /**
     *
     * @param loaded
     */
    public void setLoaded(Date loaded) {
        this.loaded = loaded;
    }

    /**
     *
     * @param aUid
     * @return
     */
    public Worker findProjectWorkersById(String aUid) {
        Worker workerFound;
        int iIndex;
        int iSize;

        workerFound = null;

        iSize = size();

        for (iIndex = 0; iIndex < iSize; iIndex++) {
            if (aUid.compareToIgnoreCase(String.valueOf(get(iIndex).getDbId())) == 0) {
                workerFound = get(iIndex);
                break;
            }

        }

        return workerFound;
    }

//    public ProjectWorkers updateProjectWorkers(ProjectWorkers aProjectWorkers) {
//        ProjectWorkers currentProjectWorkers;
//
//        currentProjectWorkers = findProjectWorkersById(String.valueOf(aProjectWorkers.getDbId()));
//
//        if (currentProjectWorkers != null) {
//            currentProjectWorkers.updateFrom(aProjectWorkers);
//            EventBus.getDefault().post(new EventArrayListProjectWorkersChange());
//            return currentProjectWorkers;
//        } else {
//            return null;
//        }
//    }
//
//    public void removeProjectWorkers(ProjectWorkers aProjectWorkers) {
//        ProjectWorkers currentProjectWorkers;
//
//        currentProjectWorkers = findProjectWorkersById(String.valueOf(aProjectWorkers.getDbId()));
//
//        if (currentProjectWorkers != null) {
//            remove(currentProjectWorkers);
//            EventBus.getDefault().post(new EventArrayListProjectWorkersChange());
//        }
//    }
//
//    @Override
//    public boolean add(ProjectWorkers aProjectWorkers) {
//        boolean result;
//        result = super.add(aProjectWorkers);
//        EventBus.getDefault().post(new EventArrayListProjectWorkersChange());
//        return result;
//    }
}
