package il.co.techschool.workmanageapp.activities.activities;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import il.co.techschool.workmanageapp.R;
import il.co.techschool.workmanageapp.activities.adapters.AdapterWorkerCompanies;
import il.co.techschool.workmanageapp.activities.common.AppWorkerPresence;

public class ActFragmentJoinedCompanies extends Fragment
{

    private static final String TAG = "JoinedProjects";
    private AppWorkerPresence mAppWorkerPresence;

    private ListView mlstFragmentJoinedCompanies;

    /**
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_joined_companies,container,false);

        mlstFragmentJoinedCompanies = (ListView)view.findViewById(R.id.lstFragmentJoinedCompanies);

        AdapterWorkerCompanies adapterWorkerCompanies;
        adapterWorkerCompanies = new AdapterWorkerCompanies(getActivity(),AppWorkerPresence.APP_INSTANCE.getArrayListWorkerCompanies());

        mlstFragmentJoinedCompanies.setAdapter(adapterWorkerCompanies);

        return view;
    }

}
