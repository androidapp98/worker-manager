package il.co.techschool.workmanageapp.activities.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 12/11/2017.
 */

public class CompanyProjects {
    @SerializedName("db_id")
    @Expose
    private String dbId;
    @SerializedName("company_id")
    @Expose
    private Integer companyId;
    @SerializedName("project_Id")
    @Expose
    private Integer projectId;

    /**
     *
     */
    public CompanyProjects(){
    }

    /**
     *
     * @param aDbId
     * @param aCompanyId
     * @param aProjectId
     */
    public CompanyProjects(String aDbId, Integer aCompanyId, Integer aProjectId) {

        dbId = aDbId;
        companyId = aCompanyId;
        projectId = aProjectId;

    }

    /**
     *
     * @return
     */
    public String getDbId() {
        return dbId;
    }

    /**
     *
     * @param aDbId
     */
    public void setDbId(String aDbId) {
        dbId = aDbId;
    }

    /**
     *
     * @return
     */
    public Integer getCompanyId() {
        return companyId;
    }

    /**
     *
     * @param aCompanyId
     */
    public void setCompanyId(Integer aCompanyId) {
        companyId = aCompanyId;
    }

    /**
     *
     * @return
     */
    public Integer getProjectId() {
        return projectId;
    }

    /**
     *
     * @param aProjectId
     */
    public void setProjectId(Integer aProjectId) {
        projectId = aProjectId;
    }

    /**
     *
     * @param aCompanyProjects
     */
    public void updateFrom(CompanyProjects aCompanyProjects) {
        dbId = aCompanyProjects.dbId;
        companyId = aCompanyProjects.companyId;
        projectId = aCompanyProjects.projectId;
    }
}
