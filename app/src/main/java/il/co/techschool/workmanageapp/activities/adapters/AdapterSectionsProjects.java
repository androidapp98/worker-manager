package il.co.techschool.workmanageapp.activities.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import il.co.techschool.workmanageapp.activities.activities.ActFragmentCreatedProjects;
import il.co.techschool.workmanageapp.activities.activities.ActFragmentJoinedProjects;

public class AdapterSectionsProjects extends FragmentPagerAdapter {

    private final List<Fragment>mFragmentList = new ArrayList<>();
    private final List<String>mFragmentTitleList = new ArrayList<>();

    /**
     *
     * @param fragment
     * @param title
     */
    public void addFragment(ActFragmentCreatedProjects fragment, String title){
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    /**
     *
     * @param fragment2
     * @param title2
     */
    public void addFragment2(ActFragmentJoinedProjects fragment2, String title2){
        mFragmentList.add(fragment2);
        mFragmentTitleList.add(title2);
    }

    /**
     *
     * @param fm
     */
    public AdapterSectionsProjects(FragmentManager fm) {
        super(fm);
    }

    /**
     *
     * @param position
     * @return
     */
    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

    /**
     *
     * @param position
     * @return
     */
    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    /**
     *
     * @return
     */
    @Override
    public int getCount() {
        return mFragmentList.size();
    }
}
