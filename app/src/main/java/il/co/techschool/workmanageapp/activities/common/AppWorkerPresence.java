package il.co.techschool.workmanageapp.activities.common;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.crashlytics.android.Crashlytics;

import il.co.techschool.workmanageapp.activities.arrays.ArrayListCompanyProjects;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListCompanyWorkers;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListLocation;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListManagerProjects;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListOwnerCompanies;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListProjectWorkers;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListWorkTimeList;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListWorkerCompanies;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListWorkerProjects;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListWorkers;
import il.co.techschool.workmanageapp.activities.db.WMDBAPI;
import il.co.techschool.workmanageapp.activities.entities.Project;
import il.co.techschool.workmanageapp.activities.entities.Worker;
import il.co.techschool.workmanageapp.activities.firebase.WMFirebase;
import io.fabric.sdk.android.Fabric;

import static il.co.techschool.workmanageapp.activities.activities.ActLogin.PhoneNumber;

/**
 * Created by Admin on 2/21/2018.
 */

@SuppressLint("Registered")
public class AppWorkerPresence extends Application {

    private WMDBAPI mWMDBAPI;
    public static AppWorkerPresence APP_INSTANCE = null;

    public String mProjectID;
    public String mWorkerID;
    public String mUserPhone;
    public String mCompanyID;

    private ArrayListCompanyWorkers mArrayListCompanyWorkers;
    private ArrayListCompanyProjects mArrayListCompanyProjects;

    private ArrayListProjectWorkers mArrayListProjectWorkers;

    private ArrayListWorkers mArrayListWorkers;

    private ArrayListWorkerProjects mArrayListWorkerProjects;
    private ArrayListWorkerCompanies mArrayListWorkerCompanies;

    private ArrayListManagerProjects mArrayListManagerProjects;
    private ArrayListOwnerCompanies mArrayListOwnerCompanies;

    private ArrayListWorkTimeList mArrayListWorkTimeList;
    private ArrayListLocation mArrayListLocation;

    private SharedPreferences sharedpreferences;

    public static final String WorkerId = "workerID";
    public static final String ProjectId = "projectID";
    public static final String CompanyId = "companyID";

    /**
     *
     */
    @Override
    public void onCreate() {
        super.onCreate();

        Fabric.with(this, new Crashlytics());

        mWMDBAPI = new WMDBAPI(this);

        mArrayListCompanyProjects = new ArrayListCompanyProjects();
        mArrayListCompanyWorkers = new ArrayListCompanyWorkers();

        mArrayListProjectWorkers = new ArrayListProjectWorkers();

        mArrayListWorkers = new ArrayListWorkers();

        mArrayListWorkerProjects = new ArrayListWorkerProjects();
        mArrayListWorkerCompanies = new ArrayListWorkerCompanies();

        mArrayListManagerProjects = new ArrayListManagerProjects();
        mArrayListOwnerCompanies = new ArrayListOwnerCompanies();

        mArrayListWorkTimeList = new ArrayListWorkTimeList();
        mArrayListLocation = new ArrayListLocation();

        SharedPreferences pref = getApplicationContext().getSharedPreferences("mypref", 0); // 0 - for private mode
        mUserPhone = pref.getString(PhoneNumber, ""); // getting String

        mWorkerID = mWMDBAPI.loadWorkerID(mUserPhone);
        saveWorkerID(mWorkerID);
        if (mWorkerID != null) {
            mArrayListWorkers = mWMDBAPI.loadWorkers();
            mArrayListWorkerProjects = mWMDBAPI.loadWorkerProjects(mWorkerID);
            mArrayListWorkerCompanies = mWMDBAPI.loadWorkerCompanies(mWorkerID);
            mArrayListManagerProjects = mWMDBAPI.loadManagerProjects(mWorkerID);
            mArrayListOwnerCompanies = mWMDBAPI.loadOwnerCompanies(mWorkerID);
            mArrayListWorkTimeList = mWMDBAPI.loadWorkTime(mWorkerID);
            mArrayListLocation = mWMDBAPI.loadWorkerLocation(Integer.parseInt(mWorkerID));
            setWorkerID(mWorkerID);
        }
        if (mProjectID != null) {
            mArrayListProjectWorkers = mWMDBAPI.loadProjectWorkers(getProjectID());
        }

        if (mCompanyID != null) {
            mArrayListCompanyWorkers = mWMDBAPI.loadCompanyWorkers(getCompanyID());
            mArrayListCompanyProjects = mWMDBAPI.loadCompanyProjects(getCompanyID());
        }
        APP_INSTANCE = this;

        registerActivityLifecycleCallbacks(new AppLifecycleTracker());
        WMFirebase.getInstance();
    }

    /**
     *
     */
    @Override
    public void onTerminate() {
        WMFirebase.getInstance().updateWorkersStatus(WMFirebase.getInstance().getCurrentUser().getUid(), 0);
        super.onTerminate();
    }

    /**
     *
     * @return
     */
    public ArrayListWorkers getArrayListWorkers(){
        return mArrayListWorkers;
    }

    /**
     *
     * @return
     */
    public ArrayListWorkerProjects getArrayListWorkerProjects() {
        return mArrayListWorkerProjects;
    }

    /**
     *
     * @return
     */
    public ArrayListWorkerCompanies getArrayListWorkerCompanies(){
        return mArrayListWorkerCompanies;
    }

    /**
     *
     * @return
     */
    public ArrayListManagerProjects getArrayListManagerProjects() {
        return mArrayListManagerProjects;
    }

    /**
     *
     * @return
     */
    public ArrayListOwnerCompanies getArrayListOwnerCompanies() {
        return mArrayListOwnerCompanies;
    }

    /**
     *
     * @return
     */
    public ArrayListWorkTimeList getArrayListWorkTimeList() {
        return mArrayListWorkTimeList;
    }

    /**
     *
     * @return
     */
    public ArrayListLocation getArrayListLocation(){ return mArrayListLocation;}

    /**
     *
     * @return
     */
    public ArrayListProjectWorkers getArrayListProjectWorkers(){
        return mArrayListProjectWorkers;
    }

    /**
     *
     * @return
     */
    public ArrayListCompanyWorkers getArrayListCompanyWorkers() {
        return mArrayListCompanyWorkers;
    }

    /**
     *
     * @return
     */
    public ArrayListCompanyProjects getArrayListCompanyProjects(){
        return mArrayListCompanyProjects;
    }

    /**
     *
     * @return
     */
    public String getWorkerID() {
        return mWorkerID;
    }

    /**
     *
     * @param aWorkerID
     */
    public void setWorkerID(String aWorkerID) {
        mWorkerID = aWorkerID;
    }

    /**
     *
     * @return
     */
    public String getProjectID() {
        return mProjectID;
    }

    /**
     *
     * @param aProjectID
     */
    public void setProjectID(String aProjectID) {

        mProjectID = aProjectID;
    }

    /**
     *
     * @return
     */
    public String getCompanyID() {
        return mCompanyID;
    }

    /**
     *
     * @param aCompanyID
     */
    public void setCompanyID(String aCompanyID) {
        mCompanyID = aCompanyID;
        saveCompanyID();
    }

    /**
     *
     * @return
     */
    public Worker getCurrentWorker() {
        return mArrayListCompanyWorkers.findWorkerById(WMFirebase.getInstance().getCurrentUser().getUid());
    }

    /**
     *
     * @return
     */
    public Project getCurrentProject() {
        return mArrayListManagerProjects.findProjectById(Integer.parseInt(WMFirebase.getInstance().getCurrentUser().getUid()));
    }

    /**
     *
     * @return
     */
    public WMDBAPI getWMDBAPI() {
        return mWMDBAPI;
    }

    /**
     *
     */
    private class AppLifecycleTracker implements ActivityLifecycleCallbacks {
        private int numStarted = 0;

        /**
         *
         * @param activity
         * @param savedInstanceState
         */
        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        }

        /**
         *
         * @param activity
         */
        @Override
        public void onActivityStarted(Activity activity) {
            numStarted++;
        }

        /**
         *
         * @param activity
         */
        @Override
        public void onActivityResumed(Activity activity) {
        }

        /**
         *
         * @param activity
         */
        @Override
        public void onActivityPaused(Activity activity) {
        }

        /**
         *
         * @param activity
         */
        // need to add status for worker job
        @Override
        public void onActivityStopped(Activity activity) {
            numStarted--;
            if (numStarted == 0) {
                //     WMFirebase.getInstance().updateWorkerStatus(WMFirebase.getInstance().getCurrentUser().getUid(), 0);
            }
        }

        /**
         *
         * @param activity
         * @param outState
         */
        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        }

        /**
         *
         * @param activity
         */
        @Override
        public void onActivityDestroyed(Activity activity) {
        }
    }

    /**
     *
     * @param aWorkerID
     */
    public void saveWorkerID(String  aWorkerID){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(WorkerId, aWorkerID);
        editor.apply();
    }

    /**
     *
     * @param aProjectID
     */
    public void saveProjectID(Integer aProjectID) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(ProjectId, aProjectID);
        editor.apply();
    }

    /**
     *
     */
    public void saveCompanyID() {

        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(CompanyId, mCompanyID);

        editor.apply();
    }
}
