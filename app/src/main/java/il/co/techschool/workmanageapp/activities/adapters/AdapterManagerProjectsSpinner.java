package il.co.techschool.workmanageapp.activities.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import il.co.techschool.workmanageapp.R;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListManagerProjects;
import il.co.techschool.workmanageapp.activities.common.AppWorkerPresence;
import il.co.techschool.workmanageapp.activities.entities.Project;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by Admin on 1/24/2018.
 */

public class AdapterManagerProjectsSpinner extends BaseAdapter
{
    public static final String ProjectID = "projectId";
    public static final String mypreference = "mypref";
    SharedPreferences sharedpreferences;
    private Context mContext;
    private ArrayListManagerProjects mArrayListManagerProjects;

    /**
     *
     * @param aContext
     * @param aArrayListManagerProjects
     */
    public AdapterManagerProjectsSpinner(Context aContext, ArrayListManagerProjects aArrayListManagerProjects)
    {
        mArrayListManagerProjects = aArrayListManagerProjects;
        mContext = aContext;
    }

    /**
     *
     * @return
     */
    @Override
    public int getCount()
    {
        return mArrayListManagerProjects.size();
    }

    /**
     *
     * @param i
     * @return
     */
    @Override
    public Object getItem(int i)
    {
        return mArrayListManagerProjects.getLoaded();
    }

    /**
     *
     * @param i
     * @return
     */
    @Override
    public long getItemId(int i)
    {
        return 0;
    }

    /**
     *
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, final View convertView, ViewGroup parent)
    {
        View resultView;
        final LayoutInflater layoutInflater;
        final Project currentProject;

        TextView txtProjectName;
        if (convertView == null)
        {
            layoutInflater = (LayoutInflater) AppWorkerPresence.APP_INSTANCE.getSystemService(LAYOUT_INFLATER_SERVICE);
            // create the custom layout to show the "workers"
            resultView = layoutInflater.inflate(R.layout.lo_item_spinner_projects, null);
            // find the objects inside the custom layout...
            txtProjectName = (TextView) resultView.findViewById((R.id.txtItmSpinnerProjectName));

            resultView.setTag(R.id.txtItmSpinnerProjectName, txtProjectName);

        } else
        {
            resultView = convertView;
            // extract all the pointers to the objects from the "tag" list.
            txtProjectName = (TextView) resultView.getTag(R.id.txtItmSpinnerProjectName);
        }

        currentProject = mArrayListManagerProjects.get(position);
        // update custom items in the view with the data from the "currentWorker";
        txtProjectName.setText(currentProject.getProjectName());

        // todo: fix the project id problem
        txtProjectName.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param v
             */
            @Override
            public void onClick(View v)
            {
                //Toast.makeText(mContext, " get click", Toast.LENGTH_SHORT).show();
               // AppWorkerPresence.APP_INSTANCE.setProjectID("");
                int projectID = currentProject.getDbId();
                AppWorkerPresence.APP_INSTANCE.setProjectID(String.valueOf(projectID));
                AppWorkerPresence.APP_INSTANCE.saveProjectID(projectID);
            }
        });

        return resultView;
    }
}
