package il.co.techschool.workmanageapp.activities.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import il.co.techschool.workmanageapp.R;
import il.co.techschool.workmanageapp.activities.adapters.AdapterProjectWorkers;
import il.co.techschool.workmanageapp.activities.common.AppWorkerPresence;


/**
 * Created by Admin on 10/14/2017.
 */

public class ActProjectWorkersList extends AppCompatActivity {

    private ListView mlstProjectWorkers;
    private Context mContext;
    private AppWorkerPresence mAppWorkerPresence;

    public Bundle getBundle = null;

    public int mProjectId;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_project_workers_list);

        mAppWorkerPresence = (AppWorkerPresence) getApplication();
        mContext = this;
        initComponents();
    }

    /**
     *
     */
    @Override
    protected void onResume() {
        super.onResume();
        getBundle = this.getIntent().getExtras();
        if (getBundle != null) {
            if (getBundle.containsKey("id")) {
                mProjectId = (getBundle.getInt("id"));
                AppWorkerPresence.APP_INSTANCE.setProjectID(String.valueOf(mProjectId));
            }
        }
        AdapterProjectWorkers adapterProjectWorkers;
        // start using the adapter after you have the data loading...
        adapterProjectWorkers = new AdapterProjectWorkers(mContext, AppWorkerPresence.APP_INSTANCE.getArrayListProjectWorkers());
        mlstProjectWorkers.setAdapter(adapterProjectWorkers);
    }

    /**
     *
     */
    private void initComponents() {
        mlstProjectWorkers = (ListView) findViewById(R.id.lstProjectWorkers);

    }
}