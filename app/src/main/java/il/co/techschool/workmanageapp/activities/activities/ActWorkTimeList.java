package il.co.techschool.workmanageapp.activities.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.TextView;

import il.co.techschool.workmanageapp.R;
import il.co.techschool.workmanageapp.activities.adapters.AdapterWorkTimeList;
import il.co.techschool.workmanageapp.activities.common.AppWorkerPresence;
import il.co.techschool.workmanageapp.activities.entities.WorkTimeList;

/**
 * Created by Admin on 10/14/2017.
 */

public class ActWorkTimeList extends AppCompatActivity {

    private TextView mtxtWrkTimeWorkingDay;
    private TextView mtxtWrkTimeEntranceWork;
    private TextView mtxtWrkTimeExitWork;
    private TextView mtxtWrkTimeWorkerProject;
    private TextView mtxtWrkTimeWorkerTotalHours;
    private ListView mListViewWorkTime;

    private Context mContext;
    private AppWorkerPresence mAppWorkerPresence;

    public Bundle getBundle = null;

    private WorkTimeList mWorkTimeList;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_work_time_list);

        mContext = this;
        mAppWorkerPresence = (AppWorkerPresence) getApplication();
        initComponents();
    }

    /**
     *
     */
    @Override
    protected void onResume() {
        super.onResume();
        // check if we got id to load the user details
        getBundle = this.getIntent().getExtras();
        if (getBundle != null) {
            if (getBundle.containsKey("ID")) {
                String id = getBundle.getString("ID");

                if (id != null) {

                }
            }
        }
        AdapterWorkTimeList adapterWorkTimeList;
        adapterWorkTimeList = new AdapterWorkTimeList(mContext, mAppWorkerPresence.getArrayListWorkTimeList());
        mListViewWorkTime.setAdapter(adapterWorkTimeList);
    }

    /**
     *
     */
    private void initComponents() {
        setContentView(R.layout.act_work_time_list);

        mtxtWrkTimeWorkingDay = (TextView) findViewById(R.id.txtWrkTimeWorkingDay);
        mtxtWrkTimeEntranceWork = (TextView) findViewById(R.id.txtWrkTimeEntranceWork);
        mtxtWrkTimeExitWork = (TextView) findViewById(R.id.txtWrkTimeExitWork);
        mtxtWrkTimeWorkerProject = (TextView) findViewById(R.id.txtWrkTimeWorkerProject);
        mtxtWrkTimeWorkerTotalHours = (TextView) findViewById(R.id.txtWrkTimeWorkerTotalHours);
        mListViewWorkTime = (ListView) findViewById(R.id.lstWorkTimeLst);


    }
}
