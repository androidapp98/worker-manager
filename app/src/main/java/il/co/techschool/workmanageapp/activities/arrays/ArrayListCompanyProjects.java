package il.co.techschool.workmanageapp.activities.arrays;

import java.util.ArrayList;
import java.util.Date;

import il.co.techschool.workmanageapp.activities.entities.Project;

/**
 * Created by Admin on 1/14/2018.
 */

public class ArrayListCompanyProjects extends ArrayList<Project> {

    private Date loaded;

    /**
     *
     * @return
     */
    public Date getLoaded() {
        return loaded;
    }

    /**
     *
     * @param loaded
     */
    public void setLoaded(Date loaded) {
        this.loaded = loaded;
    }

    /**
     *
     * @param aUid
     * @return
     */
    public Project findProjectById(Integer aUid) {
        Project projectFound;
        int iIndex;
        int iSize;

        projectFound = null;

        iSize = size();

        for (iIndex = 0; iIndex < iSize; iIndex++) {

            if (aUid.equals(get(iIndex).getDbId())) {
                projectFound = get(iIndex);
                break;
            }

        }

        return projectFound;
    }

}
