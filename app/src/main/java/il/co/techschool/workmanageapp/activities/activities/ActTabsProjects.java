package il.co.techschool.workmanageapp.activities.activities;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import il.co.techschool.workmanageapp.R;
import il.co.techschool.workmanageapp.activities.adapters.AdapterSectionsProjects;

public class ActTabsProjects extends AppCompatActivity {
    private AdapterSectionsProjects mAdapterSectionsProjects;

    private Button mbtnProjectNew;
    private ViewPager mViewPager;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_tabs_projects);

        mAdapterSectionsProjects = new AdapterSectionsProjects(getSupportFragmentManager());
        initComponents();

        initComponentsListener();
    }

    /**
     *
     */
    public void initComponents(){
        // Set up the ViewPager with the sections adapter
        mViewPager = (ViewPager)findViewById(R.id.container);
        setupViewPager(mViewPager);

        TabLayout tabLayout = (TabLayout)findViewById(R.id.projectsTabs);
        tabLayout.setupWithViewPager(mViewPager);

        mbtnProjectNew = (Button)findViewById(R.id.btnProjectNew);
    }

    /**
     *
     */
    public void initComponentsListener(){
        mbtnProjectNew.setOnClickListener(onClick);

    }

    View.OnClickListener onClick = new View.OnClickListener() {
        /**
         *
         * @param v
         */
        @Override
        public void onClick(View v)
        {
            switch (v.getId()){
                case R.id.btnProjectNew:
                    Intent actProject = new Intent(ActTabsProjects.this, ActProject.class);
                    startActivity(actProject);
                    break;
            }
        }
    };

    /**
     *
     * @param viewPager
     */
    private void setupViewPager(ViewPager viewPager){

        AdapterSectionsProjects projectsAdapter = new AdapterSectionsProjects(getSupportFragmentManager());
       projectsAdapter.addFragment(new ActFragmentCreatedProjects(), "Created Projects");


        projectsAdapter.addFragment2(new ActFragmentJoinedProjects(), "Joined Projects");

        viewPager.setAdapter(projectsAdapter);

    }

}

