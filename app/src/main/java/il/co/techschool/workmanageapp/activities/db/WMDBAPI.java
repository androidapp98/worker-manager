package il.co.techschool.workmanageapp.activities.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import java.util.Arrays;
import java.util.Date;

import il.co.techschool.workmanageapp.activities.arrays.ArrayListCompanyProjects;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListCompanyWorkers;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListLocation;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListManagerProjects;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListOwnerCompanies;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListProjectWorkers;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListWorkTimeList;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListWorkerCompanies;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListWorkerProjects;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListWorkers;
import il.co.techschool.workmanageapp.activities.common.Utils;
import il.co.techschool.workmanageapp.activities.entities.Company;
import il.co.techschool.workmanageapp.activities.entities.CompanyProjects;
import il.co.techschool.workmanageapp.activities.entities.CompanyWorkers;
import il.co.techschool.workmanageapp.activities.entities.Project;
import il.co.techschool.workmanageapp.activities.entities.ProjectWorkers;
import il.co.techschool.workmanageapp.activities.entities.WorkTimeList;
import il.co.techschool.workmanageapp.activities.entities.Worker;
import il.co.techschool.workmanageapp.activities.entities.WorkerLocation;

import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_COMPANY_ID;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_COMPANY_NAME;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_DESCRIPTION;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_DOB;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_EDUCATION_LEVEL;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_END_DATE;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_ENTER_LOCATION_ID;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_ENTER_WORK_DATE;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_EXIT_LOCATION_ID;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_EXIT_WORK_DATE;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_IMAGE;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_LATITUDE;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_LOCATION;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_LONGITUDE;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_MANAGER_ID;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_OWNER_ID;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_PHONE_NUMBER;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_PROJECT_ID;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_PROJECT_NAME;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_SALARY;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_START_DATE;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_STATUS;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_WORKER_ID;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_WORK_KIND;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_WRK_FIRST_NAME;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_WRK_ISRAELI_ID;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.FLD_WRK_LAST_NAME;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.TAG;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.TBL_COMPANIES;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.TBL_COMPANY_PROJECTS;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.TBL_COMPANY_WORKERS;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.TBL_LOCATIONS;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.TBL_PROJECTS;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.TBL_PROJECT_WORKERS;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.TBL_WORKERS;
import static il.co.techschool.workmanageapp.activities.db.WMSQLiteOpenHelper.TBL_WORKER_DAYS;

/**
 * Created by Admin on 10/15/2017.
 */

public class WMDBAPI
{

    private Context mContext;
    private WMSQLiteOpenHelper mWMSQLiteOpenHelper;
    private SQLiteDatabase mSQLiteDatabaseRW;
    private SQLiteDatabase mSQLiteDatabaseRO;

    /**
     *
     * @param aContext
     */
    public WMDBAPI(Context aContext) {
        mContext = aContext;

        mWMSQLiteOpenHelper = new WMSQLiteOpenHelper(mContext);

        mSQLiteDatabaseRO = mWMSQLiteOpenHelper.getReadableDatabase();
        mSQLiteDatabaseRW = mWMSQLiteOpenHelper.getWritableDatabase();
    }

    /**
     *
     * @param aWorker
     * @return
     */
    public boolean saveWorker(Worker aWorker) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(FLD_WRK_FIRST_NAME, aWorker.getFirstName());
        contentValues.put(FLD_WRK_LAST_NAME, aWorker.getLastName());

        String tmp = Utils.DateToString(aWorker.getDob());

        contentValues.put(FLD_DOB, tmp);
        contentValues.put(FLD_WRK_ISRAELI_ID, aWorker.getIsraeliID());
        contentValues.put(FLD_PHONE_NUMBER, aWorker.getPhoneNumber());
        contentValues.put(FLD_EDUCATION_LEVEL, aWorker.getEducationLevel());
        contentValues.put(FLD_WORK_KIND, aWorker.getWorkKind());

        long result = mSQLiteDatabaseRW.insert(TBL_WORKERS, null, contentValues);
        if (result == -1)
            return false;
        else
            return true;

    }

    /**
     *
     * @param aWorkTimeList
     * @return
     */
    public boolean saveStartWorkTime(WorkTimeList aWorkTimeList) {
        ContentValues contentValues = new ContentValues();
        String enterWork = Utils.DateTimeToString(aWorkTimeList.getEntranceWork());
        String exitWork = Utils.DateTimeToString(aWorkTimeList.getExitWork());
        contentValues.put(FLD_WORKER_ID, aWorkTimeList.getWorkerID());
        contentValues.put(FLD_ENTER_WORK_DATE, enterWork);
        contentValues.put(FLD_EXIT_WORK_DATE, exitWork);
        contentValues.put(FLD_PROJECT_ID, aWorkTimeList.getProjectID());

        long result = mSQLiteDatabaseRW.insert(TBL_WORKER_DAYS, null, contentValues);

        Log.d(TAG, String.valueOf(result));
        if (result == -1)
            return false;
        else
            return true;

    }

    /**
     *
     * @param aWorkTimeList
     * @param aWorkTimeID
     * @return
     */
    public boolean saveFinishWorkTime(WorkTimeList aWorkTimeList, Integer aWorkTimeID) {
        ContentValues contentValues = new ContentValues();
        String exitWork = Utils.DateTimeToString(aWorkTimeList.getExitWork());
        contentValues.put(FLD_EXIT_WORK_DATE, exitWork);
        long result = mSQLiteDatabaseRW.update(TBL_WORKER_DAYS,
                contentValues, BaseColumns._ID + " = " + aWorkTimeID, null);
        Log.d(TAG, String.valueOf(result));

        if (result == -1)
            return false;
        else
            return true;

    }

    /**
     *
     * @param aWorkTimeList
     * @return
     */
    public boolean updateWorkTime(WorkTimeList aWorkTimeList) {
        ContentValues contentValues = new ContentValues();
        String enterWork = Utils.DateTimeToString(aWorkTimeList.getEntranceWork());
        String exitWork = Utils.DateTimeToString(aWorkTimeList.getExitWork());

        contentValues.put(FLD_ENTER_WORK_DATE, enterWork);
        contentValues.put(FLD_EXIT_WORK_DATE, exitWork);
        long result = mSQLiteDatabaseRW.update(TBL_WORKER_DAYS,
                contentValues, BaseColumns._ID + " = " + aWorkTimeList.getDbId(), null);
        if (result == -1)
            return false;
        else
            return true;

    }

    /**
     *
     * @param aWorkerLocation
     * @return
     */
    public boolean saveWorkerLocation(WorkerLocation aWorkerLocation) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FLD_WORKER_ID, aWorkerLocation.getWorkerID());
        contentValues.put(FLD_LONGITUDE, aWorkerLocation.getLongitude());
        contentValues.put(FLD_LATITUDE, aWorkerLocation.getLatitude());

        long result = mSQLiteDatabaseRW.insert(TBL_LOCATIONS, null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }

    /**
     *
     * @param aProject
     * @return
     */
    public boolean saveProject(Project aProject) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(FLD_PROJECT_NAME, aProject.getProjectName());
        contentValues.put(FLD_MANAGER_ID, aProject.getManagerId());
        contentValues.put(FLD_LOCATION, aProject.getLocation());
        contentValues.put(FLD_DESCRIPTION, aProject.getDescription());
        contentValues.put(FLD_STATUS, aProject.getProjectStatus());
        contentValues.put(FLD_START_DATE, aProject.getStartDate().getTime());
        contentValues.put(FLD_END_DATE, aProject.getEndDate().getTime());

        if (aProject.getDbId() == null)
        {
            long result = mSQLiteDatabaseRW.insert(TBL_PROJECTS, null, contentValues);
            if (result == -1)
            {
                return false;
            } else
            {
                return true;
            }
        } else
        {
            long result = mSQLiteDatabaseRW.update(TBL_PROJECTS,
                    contentValues, BaseColumns._ID + "= " + aProject.getDbId(), null);
            if (result == -1)
            {
                return false;
            } else
            {
                return true;
            }
        }
    }

    /**
     *
     * @param aProjectWorkers
     * @return
     */
    public boolean addWorkersToProject(ProjectWorkers aProjectWorkers) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FLD_PROJECT_ID, aProjectWorkers.getProjectID());
        contentValues.put(FLD_WORKER_ID, aProjectWorkers.getWorkerID());
        contentValues.put(FLD_STATUS, aProjectWorkers.getStatus());
        contentValues.put(FLD_SALARY, aProjectWorkers.getSalary());

        long result = mSQLiteDatabaseRW.insert(TBL_PROJECT_WORKERS, null, contentValues);
        if (result == -1)
        {
            return false;
        } else
        {
            return true;
        }
    }

    /**
     *
     * @param aWorkerID
     * @param aProjectID
     * @return
     */
    public Integer removeWorkerFromProject(Integer aWorkerID, Integer aProjectID) {

        int result = mSQLiteDatabaseRW.delete(TBL_PROJECT_WORKERS
                , FLD_WORKER_ID + " = " + Arrays.toString(new String[aWorkerID]) + " AND " + FLD_PROJECT_ID +
                        " = " + Arrays.toString(new String[aProjectID]), null);
        return result;
    }

    /**
     *
     * @param aCompany
     * @return
     */
    public boolean saveCompany(Company aCompany) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(FLD_COMPANY_NAME, aCompany.getCompanyName());
        contentValues.put(FLD_LOCATION, aCompany.getLocation());
        contentValues.put(FLD_OWNER_ID, aCompany.getOwnerId());
        contentValues.put(FLD_IMAGE, aCompany.getCompanyIcon());

        if (aCompany.getDbId() == null)
        {
            long result = mSQLiteDatabaseRW.insert(TBL_COMPANIES, null, contentValues);
            if (result == -1)
            {
                return false;
            } else
            {
                return true;
            }
        } else
        {
            long result = mSQLiteDatabaseRW.update(TBL_COMPANIES,
                    contentValues, BaseColumns._ID + "= " + aCompany.getDbId(), null);
            if (result == -1)
            {
                return false;
            } else
            {
                return true;
            }
        }
    }

    /**
     *
     * @param aCompanyProjects
     * @return
     */
    public boolean addProjectToCompany(CompanyProjects aCompanyProjects){
        ContentValues contentValues = new ContentValues();
        contentValues.put(FLD_COMPANY_ID, aCompanyProjects.getCompanyId());
        contentValues.put(FLD_PROJECT_ID, aCompanyProjects.getProjectId());

        long result = mSQLiteDatabaseRW.insert(TBL_COMPANY_PROJECTS, null, contentValues);
        if (result == -1)
            return false;
        else
            return true;

    }

    /**
     *
     * @param aProjectID
     * @param aCompanyID
     * @return
     */
    public Integer removeProjectFromCompany (Integer aProjectID, Integer aCompanyID){
        int result = mSQLiteDatabaseRW.delete(TBL_COMPANY_WORKERS
                , FLD_WORKER_ID + " = " + aProjectID
                        + " AND " + FLD_COMPANY_ID + " = " + aCompanyID, null);

        return result;
    }

    /**
     *
     * @param aCompanyWorkers
     * @return
     */
    public boolean addWorkerToCompany(CompanyWorkers aCompanyWorkers) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(FLD_COMPANY_ID, aCompanyWorkers.getCompanyId());
        contentValues.put(FLD_WORKER_ID, aCompanyWorkers.getWorkerId());
        contentValues.put(FLD_SALARY, aCompanyWorkers.getSalary());

        long result = mSQLiteDatabaseRW.insert(TBL_COMPANY_WORKERS, null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }

    /**
     *
     * @param aWorkerID
     * @param aCompanyID
     * @return
     */
    public Integer removeWorkerFromCompany(Integer aWorkerID, Integer aCompanyID) {
        int result = mSQLiteDatabaseRW.delete(TBL_COMPANY_WORKERS
                , FLD_WORKER_ID + "= " + Arrays.toString(new String[aWorkerID])
                        + " AND " + FLD_COMPANY_ID + " = " + Arrays.toString(new String[aCompanyID]), null);

        // aProjectWorkers.getWorkerID()
        return result;
    }

    /**
     *
     * @param IsraeliID
     * @return
     */
    public int loadWorkerIfIsraeliIDExist(String IsraeliID) {

        String sqlSearchWorker = " select " + BaseColumns._ID + " from " + TBL_WORKERS + " where "
                + FLD_WRK_ISRAELI_ID + " =" + IsraeliID;
        Cursor res = mSQLiteDatabaseRO.rawQuery(sqlSearchWorker, new String[]{});

        if (res.getCount() == 0)
        {
            return -1;
        } else
        {
            return res.getInt(res.getColumnIndex(BaseColumns._ID));
        }
    }

    /**
     *
     * @param WorkerID
     * @return
     */
    public int loadLastStartWorkTimeID(Integer WorkerID) {
        String sqlSearchStartTime = "SELECT MAX(" + BaseColumns._ID + ") AS max_id FROM " + TBL_WORKER_DAYS + " WHERE "
                + FLD_WORKER_ID + " = " + WorkerID + ";";
        Cursor res = mSQLiteDatabaseRO.rawQuery(sqlSearchStartTime, new String[]{});
        res.moveToFirst();
        if (res.getCount() == 0) {
            return -1;
        } else {
            int columnIndex = res.getColumnIndex("max_id");
            return res.getInt(columnIndex);
        }

    }

    /**
     *
     * @param WorkerID
     * @return
     */
    public int loadLastWorkerLocationID(String WorkerID) {
        String sqlSearchLocation = "SELECT MAX(" + BaseColumns._ID + ") AS max_id FROM " + TBL_LOCATIONS + " WHERE "
                + FLD_WORKER_ID + " = " + WorkerID + ";";

        Log.d(TAG, sqlSearchLocation);

//      String sqlCheckIfDataExist = "SELECT * FROM " + TBL_LOCATIONS + " WHERE " + FLD_WORKER_ID + " = "  + WorkerID + ";";
//      Cursor cursorData = mSQLiteDatabaseRO.rawQuery( sqlCheckIfDataExist, new String[]{});
//      Log.d( TAG, String.valueOf( cursorData.getCount()));

        Cursor res = mSQLiteDatabaseRO.query(sqlSearchLocation, new String[]{"MAX(" + BaseColumns._ID + ") AS MAX"}, null, null, null, null, null);
        res.moveToFirst(); //Set the position to the beginning, current position is -1. You can check if there is something in the cursor since this return a boolean

        if (res.getCount() == 0) {
            return -1;
        } else {
            //return res.getInt(res.getColumnIndex(BaseColumns._ID));
            int columnIndex = res.getColumnIndex("max_id");
            //  int columnIndex = res.getColumnIndex(BaseColumns._ID);
            res.close();
            return res.getInt(columnIndex);
        }
    }

    /**
     *
     * @param aWorkerPhone
     * @return
     */
    public String loadWorkerID(String aWorkerPhone) {

        String sqlSearchWorker = " select " + BaseColumns._ID + " from " + TBL_WORKERS + " where "
                 + FLD_PHONE_NUMBER + " = " + "\"" + aWorkerPhone + "\"";
        Cursor res = mSQLiteDatabaseRO.rawQuery(sqlSearchWorker, new String[]{});
        if (res.getCount() == 0)
        {
            return null;
        } else
        {
            res.moveToFirst();
            return res.getString(res.getColumnIndex(BaseColumns._ID));

        }
    }

    /**
     *
     * @return
     */
    public ArrayListWorkers loadWorkers() {

        ArrayListWorkers arrayListWorkers;
        arrayListWorkers = new ArrayListWorkers();
        Cursor res = mSQLiteDatabaseRO.rawQuery("select * from " + TBL_WORKERS, null);
        res.moveToFirst();
        while (!res.isAfterLast())
        {

            String t3 = res.getString(res.getColumnIndex(FLD_DOB));
            Date tmp = Utils.StringToDate(t3);
            Worker worker = new Worker(res.getString(res.getColumnIndex(FLD_WRK_FIRST_NAME)),
                    res.getString(res.getColumnIndex(FLD_WRK_LAST_NAME)),
                    res.getString(res.getColumnIndex(FLD_WRK_ISRAELI_ID)),
                    tmp,
                    res.getString(res.getColumnIndex(BaseColumns._ID)),
                    res.getString(res.getColumnIndex(FLD_PHONE_NUMBER)),
                    res.getString(res.getColumnIndex(FLD_EDUCATION_LEVEL)),
                    res.getString(res.getColumnIndex(FLD_WORK_KIND)));
            arrayListWorkers.add(worker);
            res.moveToNext();
        }
        arrayListWorkers.setLoaded(new Date());
        return arrayListWorkers;
    }

    /**
     *
     * @param aWorkerID
     * @return
     */
    public ArrayListWorkTimeList loadWorkTime(String aWorkerID) {
        ArrayListWorkTimeList arrayListWorkTimeList;
        arrayListWorkTimeList = new ArrayListWorkTimeList();

        String query = "select * from " + TBL_WORKER_DAYS +
                " WHERE " + FLD_WORKER_ID + " = " + Integer.parseInt(aWorkerID);
        Cursor res = mSQLiteDatabaseRO.rawQuery(query, new String[]{});
        res.moveToFirst();
        while (!res.isAfterLast()) {

            String entWrk = res.getString(res.getColumnIndex(FLD_ENTER_WORK_DATE));
            String exitWrk = res.getString(res.getColumnIndex(FLD_EXIT_WORK_DATE));

            Date tmpIn = Utils.StringToDateTime(entWrk);
            Date tmpOut = Utils.StringToDateTime(exitWrk);

                WorkTimeList workTimeList = new WorkTimeList(res.getInt(res.getColumnIndex(FLD_WORKER_ID))
                        , tmpIn, tmpOut, res.getString(res.getColumnIndex(BaseColumns._ID)),
                        res.getInt(res.getColumnIndex(FLD_ENTER_LOCATION_ID)),
                        res.getInt(res.getColumnIndex(FLD_EXIT_LOCATION_ID)),
                        res.getInt(res.getColumnIndex(FLD_PROJECT_ID))
                );

                // mWorkTimeList.setEntranceWork(Utils.StringtoDateTime(res.getString(res.getColumnIndex(FLD_ENTER_WORK_DATE))));
                //  mWorkTimeList.setExitWork(Utils.StringtoDateTime(res.getString(res.getColumnIndex(FLD_EXIT_WORK_DATE))));
                arrayListWorkTimeList.add(workTimeList);

                res.moveToNext();
            }

        arrayListWorkTimeList.setLoaded(new Date());
        return arrayListWorkTimeList;
    }

    /**
     *
     * @param workerID
     * @return
     */
    // TODO: 11/25/2017 load worker location from the db..
    public ArrayListLocation loadWorkerLocation(Integer workerID) {

        ArrayListLocation arrayListLocation = new ArrayListLocation();
        String query = "select * from " + TBL_LOCATIONS + " where " +
                FLD_WORKER_ID + " = " + String.valueOf(workerID);

        Cursor res = mSQLiteDatabaseRO.rawQuery(query, new String[]{});
        res.moveToFirst();
        while (!res.isAfterLast())
        {

            WorkerLocation workerLocation = new WorkerLocation(res.getString(res.getColumnIndex(BaseColumns._ID)),
                    res.getInt(res.getColumnIndex(FLD_WORKER_ID)),
                    res.getDouble(res.getColumnIndex(FLD_LONGITUDE)),
                    res.getDouble(res.getColumnIndex(FLD_LATITUDE)));

            arrayListLocation.add(workerLocation);
            res.moveToNext();
        }
        arrayListLocation.setLoaded(new Date());
        return arrayListLocation;
    }

    /**
     *
     * @param aWorkerID
     * @return
     */
    public ArrayListManagerProjects loadManagerProjects(String aWorkerID) {
        ArrayListManagerProjects arrayListManagerProjects;
        arrayListManagerProjects = new ArrayListManagerProjects();
        String query = "select * from " + TBL_PROJECTS + " where " + FLD_MANAGER_ID + " = " + Integer.parseInt(aWorkerID);
        //     Log.d(TAG, joinQuery);

        Cursor res = mSQLiteDatabaseRO.rawQuery(query, new String[]{});
        res.moveToFirst();
        while (!res.isAfterLast())
        {

            Project project = new Project((res.getInt(res.getColumnIndex(BaseColumns._ID))),
                    res.getString(res.getColumnIndex(FLD_PROJECT_NAME)),
                    res.getInt(res.getColumnIndex(FLD_MANAGER_ID)),
                    res.getString(res.getColumnIndex(FLD_LOCATION)),
                    res.getString(res.getColumnIndex(FLD_DESCRIPTION)),
                    res.getInt(res.getColumnIndex(FLD_STATUS)),
                    new Date(res.getInt(res.getColumnIndex(FLD_START_DATE))),
                    new Date(res.getInt(res.getColumnIndex(FLD_END_DATE)))
            );
            arrayListManagerProjects.add(project);
            res.moveToNext();
        }
        arrayListManagerProjects.setLoaded(new Date());
      //  res.close();
        return arrayListManagerProjects;
    }

    /**
     *
     * @param aWorkerID
     * @return
     */
    public ArrayListWorkerProjects loadWorkerProjects(String aWorkerID) {
        ArrayListWorkerProjects arrayListWorkerProjects = new ArrayListWorkerProjects();

        String joinQuery = "select * from " + TBL_PROJECTS + " INNER JOIN " + TBL_PROJECT_WORKERS +
                " ON " + TBL_PROJECTS + "." + BaseColumns._ID + " = " + TBL_PROJECT_WORKERS + "." + FLD_PROJECT_ID + " WHERE " +
                TBL_PROJECT_WORKERS + "." + FLD_WORKER_ID + " = " + String.valueOf(aWorkerID) + " OR " + FLD_MANAGER_ID + " = " + Integer.parseInt(aWorkerID);

        Cursor res = mSQLiteDatabaseRO.rawQuery(joinQuery, new String[]{});
        res.moveToFirst();
        while (!res.isAfterLast())
        {

            Project project = new Project((res.getInt(res.getColumnIndex(BaseColumns._ID))),
                    res.getString(res.getColumnIndex(FLD_PROJECT_NAME)),
                    res.getInt(res.getColumnIndex(FLD_MANAGER_ID)),
                    res.getString(res.getColumnIndex(FLD_LOCATION)),
                    res.getString(res.getColumnIndex(FLD_DESCRIPTION)),
                    res.getInt(res.getColumnIndex(FLD_STATUS)),
                    new Date(res.getInt(res.getColumnIndex(FLD_START_DATE))),
                    new Date(res.getInt(res.getColumnIndex(FLD_END_DATE)))
            );
            arrayListWorkerProjects.add(project);
            res.moveToNext();
        }
        arrayListWorkerProjects.setLoaded(new Date());
        res.close();
        return arrayListWorkerProjects;
    }

    /**
     *
     * @param aProjectID
     * @return
     */
    public ArrayListProjectWorkers loadProjectWorkers(String aProjectID) {

        ArrayListProjectWorkers arrayListProjectWorkers = new ArrayListProjectWorkers();
        String joinQuery = "select * from " + TBL_WORKERS + " INNER JOIN " + TBL_PROJECT_WORKERS +
                " ON " + TBL_WORKERS + "." + BaseColumns._ID + " = " + TBL_PROJECT_WORKERS + "." + BaseColumns._ID + " WHERE " +
                TBL_PROJECT_WORKERS + "." + FLD_COMPANY_ID + " = " + String.valueOf(aProjectID);
        Cursor res = mSQLiteDatabaseRO.rawQuery(joinQuery, new String[]{});
        res.moveToFirst();
        while (!res.isAfterLast())
        {

            String t3 = res.getString(res.getColumnIndex(FLD_DOB));
            Date tmp = Utils.StringToDate(t3);
            Worker worker = new Worker(res.getString(res.getColumnIndex(FLD_WRK_FIRST_NAME)),
                    res.getString(res.getColumnIndex(FLD_WRK_LAST_NAME)),
                    res.getString(res.getColumnIndex(FLD_WRK_ISRAELI_ID)),
                    tmp,
                    res.getString(res.getColumnIndex(BaseColumns._ID)),
                    res.getString(res.getColumnIndex(FLD_PHONE_NUMBER)),
                    res.getString(res.getColumnIndex(FLD_EDUCATION_LEVEL)),
                    res.getString(res.getColumnIndex(FLD_WORK_KIND)));
            arrayListProjectWorkers.add(worker);
            res.moveToNext();
        }
        arrayListProjectWorkers.setLoaded(new Date());
        res.close();

        return arrayListProjectWorkers;
    }

    /**
     *
     * @param aOwnerID
     * @return
     */
    public ArrayListOwnerCompanies loadOwnerCompanies(String aOwnerID) {

        ArrayListOwnerCompanies arrayListOwnerCompanies = new ArrayListOwnerCompanies();
        String query = "select * from " + TBL_COMPANIES + " WHERE " + FLD_OWNER_ID + " = " + Integer.parseInt(aOwnerID);
        Cursor res = mSQLiteDatabaseRO.rawQuery(query, new String[]{});

        //Cursor res = mSQLiteDatabaseRO.rawQuery("select * from " + TBL_COMPANIES, null);
        res.moveToFirst();
        while (!res.isAfterLast())
        {
            Company company = new Company((res.getString(res.getColumnIndex(BaseColumns._ID))),
                    res.getString(res.getColumnIndex(FLD_COMPANY_NAME)),
                    res.getInt(res.getColumnIndex(FLD_OWNER_ID)),
                    res.getString(res.getColumnIndex(FLD_LOCATION)),
                    res.getBlob(res.getColumnIndex(FLD_IMAGE))
            );

            arrayListOwnerCompanies.add(company);
            res.moveToNext();
        }
        arrayListOwnerCompanies.setLoaded(new Date());
        return arrayListOwnerCompanies;
    }

    /**
     *
     * @param aWorkerID
     * @return
     */
    public ArrayListWorkerCompanies loadWorkerCompanies(String aWorkerID) {

        ArrayListWorkerCompanies arrayListWorkerCompanies = new ArrayListWorkerCompanies();
        String joinQuery = "select * from " + TBL_COMPANIES + " INNER JOIN " + TBL_COMPANY_WORKERS +
                " ON " + TBL_COMPANIES + "." + BaseColumns._ID + " = " + TBL_COMPANY_WORKERS + "." + FLD_COMPANY_ID + " WHERE " +
                TBL_COMPANY_WORKERS + "." + FLD_COMPANY_ID + " = " + String.valueOf(aWorkerID);
        Cursor res = mSQLiteDatabaseRO.rawQuery(joinQuery, new String[]{});

        //Cursor res = mSQLiteDatabaseRO.rawQuery("select * from " + TBL_COMPANIES, null);
        res.moveToFirst();
        while (!res.isAfterLast())
        {

            Company company = new Company((res.getString(res.getColumnIndex(BaseColumns._ID))),
                    res.getString(res.getColumnIndex(FLD_COMPANY_NAME)),
                    res.getInt(res.getColumnIndex(FLD_OWNER_ID)),
                    res.getString(res.getColumnIndex(FLD_LOCATION)),
                    res.getBlob(res.getColumnIndex(FLD_IMAGE))
            );

            arrayListWorkerCompanies.add(company);
            res.moveToNext();
        }
        arrayListWorkerCompanies.setLoaded(new Date());
        return arrayListWorkerCompanies;
    }

    /**
     *
     * @param aCompanyID
     * @return
     */
    public ArrayListCompanyWorkers loadCompanyWorkers(String aCompanyID) {
        ArrayListCompanyWorkers arrayListCompanyWorkers = new ArrayListCompanyWorkers();

        String joinQuery = "select * from " + TBL_WORKERS + " INNER JOIN " + TBL_COMPANY_WORKERS +
                " ON " + TBL_WORKERS + "." + BaseColumns._ID + " = " + TBL_COMPANY_WORKERS + "." + BaseColumns._ID + " WHERE " +
                TBL_COMPANY_WORKERS + "." + FLD_COMPANY_ID + " = " + String.valueOf(aCompanyID);
        Cursor res = mSQLiteDatabaseRO.rawQuery(joinQuery, new String[]{});
        res.moveToFirst();
        while (!res.isAfterLast())
        {
            String t3 = res.getString(res.getColumnIndex(FLD_DOB));
            Date tmp = Utils.StringToDate(t3);
            Worker worker = new Worker(res.getString(res.getColumnIndex(FLD_WRK_FIRST_NAME)),
                    res.getString(res.getColumnIndex(FLD_WRK_LAST_NAME)),
                    res.getString(res.getColumnIndex(FLD_WRK_ISRAELI_ID)),
                    tmp,
                    res.getString(res.getColumnIndex(BaseColumns._ID)),
                    res.getString(res.getColumnIndex(FLD_PHONE_NUMBER)),
                    res.getString(res.getColumnIndex(FLD_EDUCATION_LEVEL)),
                    res.getString(res.getColumnIndex(FLD_WORK_KIND)));
            arrayListCompanyWorkers.add(worker);
            res.moveToNext();
        }
        arrayListCompanyWorkers.setLoaded(new Date());
        res.close();

        return arrayListCompanyWorkers;
    }

    /**
     *
     * @param aCompanyID
     * @return
     */
    public ArrayListCompanyProjects loadCompanyProjects(String aCompanyID) {
        ArrayListCompanyProjects arrayListCompanyProjects = new ArrayListCompanyProjects();
        String joinQuery = "select * from " + TBL_PROJECTS + " INNER JOIN " + TBL_COMPANY_PROJECTS +
                " ON " + TBL_PROJECTS + "." + BaseColumns._ID + " = " + TBL_COMPANY_PROJECTS + "." + BaseColumns._ID + " WHERE " +
                TBL_COMPANY_PROJECTS + "." + FLD_COMPANY_ID + " = " + String.valueOf(aCompanyID);
        Cursor res = mSQLiteDatabaseRO.rawQuery(joinQuery, new String[]{});
        res.moveToFirst();
        while (!res.isAfterLast())
        {

            Project project = new Project((res.getInt(res.getColumnIndex(BaseColumns._ID))),
                    res.getString(res.getColumnIndex(FLD_PROJECT_NAME)),
                    res.getInt(res.getColumnIndex(FLD_MANAGER_ID)),
                    res.getString(res.getColumnIndex(FLD_LOCATION)),
                    res.getString(res.getColumnIndex(FLD_DESCRIPTION)),
                    res.getInt(res.getColumnIndex(FLD_STATUS)),
                    new Date(res.getInt(res.getColumnIndex(FLD_START_DATE))),
                    new Date(res.getInt(res.getColumnIndex(FLD_END_DATE)))
            );
            arrayListCompanyProjects.add(project);
            res.moveToNext();
        }
        arrayListCompanyProjects.setLoaded(new Date());
        res.close();

        return arrayListCompanyProjects;
    }
}
