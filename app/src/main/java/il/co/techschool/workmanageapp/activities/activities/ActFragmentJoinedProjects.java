package il.co.techschool.workmanageapp.activities.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import il.co.techschool.workmanageapp.R;
import il.co.techschool.workmanageapp.activities.adapters.AdapterWorkerProjects;
import il.co.techschool.workmanageapp.activities.common.AppWorkerPresence;


public class ActFragmentJoinedProjects extends Fragment {

    private static final String TAG = "Tab2JoinedProjects";
    private AppWorkerPresence mAppWorkerPresence;
    private ListView mlstFragmentJoinedProjects;

    /**
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_joined_projects,container,false);

        mlstFragmentJoinedProjects = (ListView)view.findViewById(R.id.lstFragmentJoinedProjects);

        AdapterWorkerProjects adapterWorkerProjects;
        adapterWorkerProjects = new AdapterWorkerProjects(getActivity(),AppWorkerPresence.APP_INSTANCE.getArrayListWorkerProjects());

        mlstFragmentJoinedProjects.setAdapter(adapterWorkerProjects);

        return view;
    }
}
