package il.co.techschool.workmanageapp.activities.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.Date;

import il.co.techschool.workmanageapp.R;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListWorkTimeList;
import il.co.techschool.workmanageapp.activities.common.AppWorkerPresence;
import il.co.techschool.workmanageapp.activities.common.Utils;
import il.co.techschool.workmanageapp.activities.entities.WorkTimeList;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by Admin on 10/16/2017.
 */

public class AdapterWorkTimeList extends BaseAdapter {

    private Context mContext;
    private ArrayListWorkTimeList mArrayListWorkTimeList;

    /**
     *
     * @param aContext
     * @param aArrayListWorkTimeList
     */
    public AdapterWorkTimeList(Context aContext, ArrayListWorkTimeList aArrayListWorkTimeList) {
        mArrayListWorkTimeList = aArrayListWorkTimeList;
        mContext = aContext;
    }

    /**
     *
     * @return
     */
    @Override
    public int getCount() {
        return mArrayListWorkTimeList.size();
    }

    /**
     *
     * @param position
     * @return
     */
    @Override
    public Object getItem(int position) {
        return mArrayListWorkTimeList.get(position);
    }

    /**
     *
     * @param i
     * @return
     */
    @Override
    public long getItemId(int i) {
        return 0;
    }

    /**
     *
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View resultView;
        LayoutInflater layoutInflater;
        final WorkTimeList currentWorkTime;

        TextView txtWorkerDay;
        TextView txtWorkerEnterWork;
        TextView txtWorkerExitWork;
        TextView txtProjectName;
        TextView txtTotalHours;
        ImageButton imgBtnEditTime;

        if (convertView == null) {
            layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            // create the custom layout to show the "workers"
            resultView = layoutInflater.inflate(R.layout.lo_item_work_time, null);

            // find the objects inside the custom layout...
            txtWorkerDay = (TextView) resultView.findViewById((R.id.txtItmWrkDay));
            txtWorkerEnterWork = (TextView) resultView.findViewById((R.id.txtItmWrkEntranceWork));
            txtWorkerExitWork = (TextView) resultView.findViewById((R.id.txtItmWrkExitWrk));
            txtProjectName = (TextView) resultView.findViewById((R.id.txtItmWrkProject));
            txtTotalHours = (TextView) resultView.findViewById((R.id.txtItmWrkTotalHours));
            imgBtnEditTime = (ImageButton) resultView.findViewById((R.id.imgBtnWrkTimeLst));

            // add all the "objects" to the "tag" list
            //resultView.setTag( R.id.<item it>, itemObject);
            resultView.setTag(R.id.txtItmWrkDay, txtWorkerDay);
            resultView.setTag(R.id.txtItmWrkEntranceWork, txtWorkerEnterWork);
            resultView.setTag(R.id.txtItmWrkExitWrk, txtWorkerExitWork);
            resultView.setTag(R.id.txtItmWrkProject, txtProjectName);
            resultView.setTag(R.id.txtItmWrkTotalHours, txtTotalHours);
            resultView.setTag(R.id.imgBtnWrkTimeLst, imgBtnEditTime);

        } else {
            resultView = convertView;

            // extract all the pointers to the objects from the "tag" list.
            txtWorkerDay = (TextView) resultView.getTag(R.id.txtItmWrkDay);
            txtWorkerEnterWork = (TextView) resultView.getTag(R.id.txtItmWrkEntranceWork);
            txtWorkerExitWork = (TextView) resultView.getTag(R.id.txtItmWrkExitWrk);
            txtProjectName = (TextView) resultView.getTag(R.id.txtItmWrkProject);
            txtTotalHours = (TextView) resultView.getTag(R.id.txtItmWrkTotalHours);
            imgBtnEditTime = (ImageButton) resultView.getTag(R.id.imgBtnWrkTimeLst);
        }

        currentWorkTime = mArrayListWorkTimeList.get(position);

        Date timeStart = currentWorkTime.getEntranceWork();
        String startHour = String.valueOf(timeStart.getHours());
        String startMinute = String.valueOf(timeStart.getMinutes());
        // update custom items in the view with the data from the "currentWorker";
        Date timeFinish = currentWorkTime.getExitWork();
        String finishHour = String.valueOf(timeFinish.getHours());
        String finishMinute = String.valueOf(timeFinish.getMinutes());


        int day = timeStart.getDay();
        switch (day){
            case 0:
                txtWorkerDay.setText("\"Sun\"");
                break;
            case 1:
                txtWorkerDay.setText("\"Mon\"");
                break;
            case 2:
                txtWorkerDay.setText("\"Tues\"");
                break;
            case 3:
                txtWorkerDay.setText("\"Wed\"");
                break;
            case 4:
                txtWorkerDay.setText("\"Thur\"");
                break;
            case 5:
                txtWorkerDay.setText("\"Fri\"");
                break;
            case 6:
                txtWorkerDay.setText("\"Sat\"");
                break;
        }

          //  txtWorkerDay.setText();
        int StartMinute = timeStart.getMinutes();
        int FinishMinute = timeFinish.getMinutes();
        if(StartMinute <10 && StartMinute > 0){
            txtWorkerEnterWork.setText(startHour + ":" + "0" + startMinute);
        }else
        {
            txtWorkerEnterWork.setText(startHour + ":" + startMinute);
        }
        if (FinishMinute < 10 && FinishMinute > 0){
            txtWorkerExitWork.setText(finishHour + ":" + "0" + finishMinute);
        }else {
            txtWorkerExitWork.setText(finishHour + ":" + finishMinute);
        }
        txtWorkerExitWork.setText(finishHour + ":" + finishMinute);
        txtProjectName.setText(String.valueOf(currentWorkTime.getProjectID()));
        txtTotalHours.setText(String.valueOf(timeStart.getHours() - timeFinish.getHours()));

        try {
            imgBtnEditTime.setOnClickListener(new View.OnClickListener() {
                /**
                 *
                 * @param v
                 */
                @Override
                public void onClick(View v) {
                    switch (v.getId()) {
                        case R.id.imgBtnWrkTimeLst:

                            PopupMenu popup = new PopupMenu(mContext, v);
                            popup.getMenuInflater().inflate(R.menu.popup_menu_act_work_time_list,
                                    popup.getMenu());
                            popup.show();
                            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                /**
                                 *
                                 * @param item
                                 * @return
                                 */
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {
                                    switch (item.getItemId()) {
                                        case R.id.action_edit:

                                            int workTimeID = Integer.parseInt(currentWorkTime.getDbId());
                                            editWorkTimeDialog(workTimeID);
                                            break;
                                        default:
                                            break;
                                    }
                                    return true;
                                }
                            });

                            break;
                        default:
                            break;
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultView;
    }

    /**
     *
     * @param aWorkTime
     */
    private void editWorkTimeDialog(final int aWorkTime) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View dialogView = inflater.inflate(R.layout.act_work_time_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText metWrkTimeDialogStart = (EditText) dialogView.findViewById(R.id.etWrkTimeDialogStart);
        final EditText metWrkTimeDialogFinish = (EditText) dialogView.findViewById(R.id.etWrkTimeDialogFinish);

        dialogBuilder.setTitle("Custom dialog");
        dialogBuilder.setMessage("Enter text below");
        dialogBuilder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            /**
             *
             * @param dialog
             * @param whichButton
             */
            public void onClick(DialogInterface dialog, int whichButton) {

                WorkTimeList workTimeList = new WorkTimeList();

                String enterWorkHour = metWrkTimeDialogStart.getText().toString();
                String exitWorkHour = metWrkTimeDialogFinish.getText().toString();

                    int workTimeID = aWorkTime;
                    Date startTime = Utils.StringToDateTime(enterWorkHour);
                    Date finishTime = Utils.StringToDateTime(exitWorkHour);

                    workTimeList.setEntranceWork(startTime);
                    workTimeList.setEntranceWork(finishTime);

                    AppWorkerPresence.APP_INSTANCE.getWMDBAPI().updateWorkTime(workTimeList);

            }
        });

        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            /**
             *
             * @param dialog
             * @param whichButton
             */
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }
}