package il.co.techschool.workmanageapp.activities.arrays;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Date;

import il.co.techschool.workmanageapp.activities.entities.Worker;
import il.co.techschool.workmanageapp.activities.events.EventArrayListWorkersChange;


/**
 * Created by Admin on 1/19/2018.
 */

public class ArrayListWorkers extends ArrayList<Worker> {

    private Date loaded;

    /**
     *
     * @return
     */
    public Date getLoaded() {
        return loaded;
    }

    /**
     *
     * @param loaded
     */
    public void setLoaded(Date loaded) {
        this.loaded = loaded;
    }

    /**
     *
     * @param aUid
     * @return
     */
    public Worker findWorkerById(String aUid) {
        Worker workerFound;
        int iIndex;
        int iSize;

        workerFound = null;

        iSize = size();

        for (iIndex = 0; iIndex < iSize; iIndex++) {


            if (aUid.compareToIgnoreCase(get(iIndex).getDbId()) == 0) {
                workerFound = get(iIndex);
                break;
            }

        }

        return workerFound;
    }

    /**
     *
     * @param aWorker
     * @return
     */
    public Worker updateWorker(Worker aWorker) {
        Worker currentWorker;

        currentWorker = findWorkerById(aWorker.getDbId());

        if (currentWorker != null) {
            currentWorker.updateFrom(aWorker);
            EventBus.getDefault().post(new EventArrayListWorkersChange());
            return currentWorker;
        } else {
            return null;
        }
    }

    /**
     *
     * @param aWorker
     */
    public void removeWorker(Worker aWorker) {
        Worker currentWorker;

        currentWorker = findWorkerById(aWorker.getDbId());

        if (currentWorker != null) {
            remove(currentWorker);
            EventBus.getDefault().post(new EventArrayListWorkersChange());
        }
    }

    /**
     *
     * @param aWorker
     * @return
     */
    @Override
    public boolean add(Worker aWorker) {
        boolean result;
        result = super.add(aWorker);
        EventBus.getDefault().post(new EventArrayListWorkersChange());
        return result;
    }
}
