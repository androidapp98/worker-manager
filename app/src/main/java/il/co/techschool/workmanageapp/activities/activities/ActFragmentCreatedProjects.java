package il.co.techschool.workmanageapp.activities.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import il.co.techschool.workmanageapp.R;
import il.co.techschool.workmanageapp.activities.adapters.AdapterManagerProjects;
import il.co.techschool.workmanageapp.activities.common.AppWorkerPresence;

public class ActFragmentCreatedProjects extends Fragment
{
    private static final String TAG = "Created Projects";
    private Context mContext;
    private ListView mlstFragmentCreatedProjects;

    /**
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_created_projects,container,false);

        mlstFragmentCreatedProjects = (ListView)view.findViewById(R.id.lstFragmentCreatedProjects);

        AdapterManagerProjects adapterManagerProjects;
        adapterManagerProjects = new AdapterManagerProjects(getActivity(), AppWorkerPresence.APP_INSTANCE.getArrayListManagerProjects());

        mlstFragmentCreatedProjects.setAdapter(adapterManagerProjects);

        return view;
    }
}
