package il.co.techschool.workmanageapp.activities.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;

import il.co.techschool.workmanageapp.R;
import il.co.techschool.workmanageapp.activities.activities.ActWorkerDetails;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListWorkers;
import il.co.techschool.workmanageapp.activities.common.AppWorkerPresence;
import il.co.techschool.workmanageapp.activities.entities.Worker;

/**
 * Created by Admin on 1/19/2018.
 */

public class AdapterWorkers extends BaseAdapter {

    private TextView mtxtWorkerName;
    private TextView mtxtWorkerID;
    private ImageButton mimgBtnWorker;

    private Context mContext;
    private ArrayListWorkers mArrayListWorkers;

    /**
     *
     * @param aContext
     * @param aArrayListWorkers
     */
    public AdapterWorkers(Context aContext, ArrayListWorkers aArrayListWorkers) {
        mArrayListWorkers = aArrayListWorkers;
        mContext = aContext;
    }

    /**
     *
     * @return
     */
    @Override
    public int getCount() {
        return mArrayListWorkers.size();
    }

    /**
     *
     * @param i
     * @return
     */
    @Override
    public Object getItem(int i) {
        return mArrayListWorkers.getLoaded();
    }

    /**
     *
     * @param i
     * @return
     */
    @Override
    public long getItemId(int i) {
        return 0;
    }

    /**
     *
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View resultView;
        LayoutInflater layoutInflater;
        final Worker currentWorker;

        TextView txtWorkerName;
        TextView txtWorkerID;
        ImageButton imgBtnWorker;

        if (convertView == null) {
            layoutInflater = (LayoutInflater) AppWorkerPresence.APP_INSTANCE.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            // create the custom layout to show the "workers"
            resultView = layoutInflater.inflate(R.layout.lo_item_worker, null);

            // find the objects inside the custom layout...
            txtWorkerName = (TextView) resultView.findViewById((R.id.txtItmWrkName));
            txtWorkerID = (TextView) resultView.findViewById((R.id.txtItmWrkID));
            imgBtnWorker = (ImageButton) resultView.findViewById((R.id.imgBtnItmWorker));

            // add all the "objects" to the "tag" list
            //resultView.setTag( R.id.<item it>, itemObject);

            resultView.setTag(R.id.txtItmWrkName, txtWorkerName);
            resultView.setTag(R.id.txtItmWrkID, txtWorkerID);
            resultView.setTag(R.id.imgBtnItmWorker, imgBtnWorker);

        } else {
            resultView = convertView;

            // extract all the pointers to the objects from the "tag" list.
            txtWorkerName = (TextView) resultView.getTag(R.id.txtItmWrkName);
            txtWorkerID = (TextView) resultView.getTag(R.id.txtItmWrkID);
            imgBtnWorker = (ImageButton) resultView.getTag(R.id.imgBtnItmWorker);
        }

        currentWorker = mArrayListWorkers.get(position);

        // update custom items in the view with the data from the "currentWorker";
        txtWorkerName.setText(currentWorker.getFirstName());
//        txtWorkerID.setText(currentWorker.getDbId());
        txtWorkerName.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param v
             */
            @Override
            public void onClick(View v) {
                // Toast.makeText(mContext, " get click", Toast.LENGTH_SHORT).show();

                Bundle dataBundle = new Bundle();
                dataBundle.putString("id", currentWorker.getDbId());
                Intent intent = new Intent(mContext, ActWorkerDetails.class);
                intent.putExtras(dataBundle);
                mContext.startActivity(intent);

            }
        });
        try {
            imgBtnWorker.setOnClickListener(new View.OnClickListener() {
                /**
                 *
                 * @param v
                 */
                @Override
                public void onClick(View v) {

                    switch (v.getId()) {
                        case R.id.imgBtnItmWorker:

                            PopupMenu popup = new PopupMenu(mContext, v);
                            popup.getMenuInflater().inflate(R.menu.popup_menu_act_workers_list,
                                    popup.getMenu());
                            popup.show();
                            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                /**
                                 *
                                 * @param item
                                 * @return
                                 */
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {
                                    switch (item.getItemId()) {
//
//                                        case R.id.action_show_on_map:
//
//                                            //Or Some other code you want to put here.. This is just an example.
//                                            //  Toast.makeText(mContext, " Install Clicked at position " + " : " + position, Toast.LENGTH_LONG).show();
//
//                                            Intent chooser;
//                                            Intent i = new Intent(Intent.ACTION_VIEW);
//                                            String map = ("" + i);
//                                            i.setData(Uri.parse("geo:" + i));
//                                            chooser = Intent.createChooser(i, "launch maps");
//
//                                            mContext.startActivity(chooser);
//                                            break;
                                        case R.id.action_work_time_list:

                                            return true;

                                        case R.id.action_edit:
                                            Bundle dataBundle = new Bundle();
                                            dataBundle.putString("id", currentWorker.getDbId());
                                            Intent intent = new Intent(mContext, ActWorkerDetails.class);
                                            intent.putExtras(dataBundle);
                                            mContext.startActivity(intent);

                                            break;
                                        case R.id.action_delete:

                                            mtxtWorkerName.setVisibility(View.GONE);
                                            mtxtWorkerID.setVisibility(View.GONE);
                                            mimgBtnWorker.setVisibility(View.GONE);

                                            break;
                                        default:
                                            break;
                                    }

                                    return true;
                                }
                            });

                            break;

                        default:
                            break;
                    }

                }
            });

        } catch (Exception e) {

            e.printStackTrace();
        }
        return resultView;
    }
}
