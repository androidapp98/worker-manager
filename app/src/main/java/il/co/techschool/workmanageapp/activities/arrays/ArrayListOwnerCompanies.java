package il.co.techschool.workmanageapp.activities.arrays;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Date;

import il.co.techschool.workmanageapp.activities.entities.Company;
import il.co.techschool.workmanageapp.activities.events.EventArrayListCompanyChange;

/**
 * Created by Admin on 12/12/2017.
 */

public class ArrayListOwnerCompanies extends ArrayList<Company> {

    private Date loaded;

    /**
     *
     * @return
     */
    public Date getLoaded() {
        return loaded;
    }

    /**
     *
     * @param loaded
     */
    public void setLoaded(Date loaded) {
        this.loaded = loaded;
    }

    /**
     *
     * @param aUid
     * @return
     */
    public Company findCompanyById(String aUid) {
        Company companyFound;
        int iIndex;
        int iSize;

        companyFound = null;

        iSize = size();

        for (iIndex = 0; iIndex < iSize; iIndex++) {

            if (aUid.compareToIgnoreCase(get(iIndex).getDbId()) == 0) {
                companyFound = get(iIndex);
                break;
            }
        }

        return companyFound;
    }

    /**
     *
     * @param aCompany
     * @return
     */
    public Company updateCompany(Company aCompany) {
        Company currentCompany;

        currentCompany = findCompanyById(aCompany.getDbId());

        if (currentCompany != null) {
            currentCompany.updateFrom(aCompany);
            EventBus.getDefault().post(new EventArrayListCompanyChange());
            return currentCompany;
        } else {
            return null;
        }
    }

    /**
     *
     * @param aCompany
     */
    public void removeCompany(Company aCompany) {
        Company currentCompany;

        currentCompany = findCompanyById(aCompany.getDbId());

        if (currentCompany != null) {
            remove(currentCompany);
            EventBus.getDefault().post(new EventArrayListCompanyChange());
        }
    }

    /**
     *
     * @param aCompany
     * @return
     */
    @Override
    public boolean add(Company aCompany) {
        boolean result;
        result = super.add(aCompany);
        EventBus.getDefault().post(new EventArrayListCompanyChange());
        return result;
    }
}

