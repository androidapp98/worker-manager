package il.co.techschool.workmanageapp.activities.common;

/**
 * Created by Admin on 12/25/2017.
 */

public enum WorkerStatus {
    WS_WORKER,
    WS_MANAGER
}
