package il.co.techschool.workmanageapp.activities.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import il.co.techschool.workmanageapp.R;
import il.co.techschool.workmanageapp.activities.activities.ActProject;
import il.co.techschool.workmanageapp.activities.arrays.ArrayListWorkerProjects;
import il.co.techschool.workmanageapp.activities.common.AppWorkerPresence;
import il.co.techschool.workmanageapp.activities.entities.Project;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by Admin on 1/15/2018.
 */

public class AdapterWorkerProjects extends BaseAdapter {

    private ActProject mActProject;
    private Context mContext;
    private ArrayListWorkerProjects mArrayListWorkerProjects;

    /**
     *
     * @param aContext
     * @param aArrayListWorkerProjects
     */
    public AdapterWorkerProjects(Context aContext, ArrayListWorkerProjects aArrayListWorkerProjects) {

        mArrayListWorkerProjects = aArrayListWorkerProjects;
        mContext = aContext;
    }

    /**
     *
     * @return
     */
    @Override
    public int getCount()
    {
        return mArrayListWorkerProjects.size();
    }

    /**
     *
     * @param i
     * @return
     */
    @Override
    public Object getItem(int i) {
        return mArrayListWorkerProjects.getLoaded();
    }

    /**
     *
     * @param i
     * @return
     */
    @Override
    public long getItemId(int i) {
        return 0;
    }

    /**
     *
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {
        View resultView;
        final LayoutInflater layoutInflater;
        final Project currentProject;

        TextView txtProjectName;
        TextView txtProjectID;
        ImageView imgProjectIcon;
        ImageButton imgBtnProject;

        if (convertView == null) {
            layoutInflater = (LayoutInflater) AppWorkerPresence.APP_INSTANCE.getSystemService(LAYOUT_INFLATER_SERVICE);
            // create the custom layout to show the "workers"
            resultView = layoutInflater.inflate(R.layout.lo_item_project, null);
            // find the objects inside the custom layout...
            txtProjectName = (TextView) resultView.findViewById((R.id.txtItmProjectName));
            txtProjectID = (TextView) resultView.findViewById((R.id.txtItmProjectID));
            imgBtnProject = (ImageButton) resultView.findViewById((R.id.imgBtnItmProject));

            resultView.setTag(R.id.txtItmProjectName, txtProjectName);
            resultView.setTag(R.id.txtItmProjectID, txtProjectID);
            resultView.setTag(R.id.imgBtnItmProject, imgBtnProject);

        } else {
            resultView = convertView;
            // extract all the pointers to the objects from the "tag" list.
            txtProjectName = (TextView) resultView.getTag(R.id.txtItmProjectName);
            txtProjectID = (TextView) resultView.getTag(R.id.txtItmProjectID);
            imgBtnProject = (ImageButton) resultView.getTag(R.id.imgBtnItmProject);
        }
        currentProject = mArrayListWorkerProjects.get(position);

        // update custom items in the view with the data from the "currentWorker";
        txtProjectName.setText(currentProject.getProjectName());
        //    txtProjectStatus.setText(String.valueOf(currentProject.getProjectStatus()));


        txtProjectName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext, " get click", Toast.LENGTH_SHORT).show();

                Bundle dataBundle = new Bundle();
                dataBundle.putInt("id", currentProject.getDbId());
                Intent intent = new Intent(mContext, ActProject.class);
                intent.putExtras(dataBundle);
                mContext.startActivity(intent);

            }
        });
        return resultView;
    }

}
