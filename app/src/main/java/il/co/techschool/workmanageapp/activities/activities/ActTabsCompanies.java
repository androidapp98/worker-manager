package il.co.techschool.workmanageapp.activities.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import il.co.techschool.workmanageapp.R;
import il.co.techschool.workmanageapp.activities.adapters.AdapterSectionsCompanies;


public class ActTabsCompanies extends AppCompatActivity
{
    private AdapterSectionsCompanies mAdapterSectionsCompanies;

    private Button mbtnCompanyNew;
    private ViewPager mViewPager;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_tabs_companies);

        mAdapterSectionsCompanies = new AdapterSectionsCompanies(getSupportFragmentManager());

        initComponents();

        initComponentsListener();
    }

    /**
     *
     */
    public void initComponents(){
        // Set up the ViewPager with the sections adapter
        mViewPager = (ViewPager)findViewById(R.id.container);
        setupViewPager(mViewPager);

        TabLayout tabLayout = (TabLayout)findViewById(R.id.companiesTabs);
        tabLayout.setupWithViewPager(mViewPager);

        mbtnCompanyNew = (Button)findViewById(R.id.btnCompanyNew);
    }

    /**
     *
     */
    public void initComponentsListener(){
        mbtnCompanyNew.setOnClickListener(onClick);

    }

    View.OnClickListener onClick = new View.OnClickListener() {
        /**
         *
         * @param v
         */
        @Override
        public void onClick(View v)
        {
            switch (v.getId()){
                case R.id.btnCompanyNew:
                    Intent actCompany = new Intent(ActTabsCompanies.this, ActCompany.class);
                    startActivity(actCompany);
                    break;
            }
        }
    };

    /**
     *
     * @param viewPager
     */
    private void setupViewPager(ViewPager viewPager){
        AdapterSectionsCompanies companiesAdapter = new AdapterSectionsCompanies(getSupportFragmentManager());
        companiesAdapter.addFragment(new ActFragmentsCreatedCompanies(), "Created_Companies");

        companiesAdapter.addFragment2(new ActFragmentJoinedCompanies(), "Joined_Companies");

        viewPager.setAdapter(companiesAdapter);

    }

}
