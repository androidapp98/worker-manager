package il.co.techschool.workmanageapp.activities.firebase;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import il.co.techschool.workmanageapp.activities.common.AppWorkerPresence;
import il.co.techschool.workmanageapp.activities.entities.Project;
import il.co.techschool.workmanageapp.activities.entities.WorkTimeList;
import il.co.techschool.workmanageapp.activities.entities.Worker;

/**
 * Created by Admin on 10/25/2017.
 */

public class WMFirebase {
    private static final String TAG = "WMFirebase";
    private FirebaseDatabase mDatabase;
    private DatabaseReference mDBReferenceWorkers;
    private DatabaseReference mDBReferenceWorkTimeList;
    private DatabaseReference mDBReferenceProjects;
    private DatabaseReference mDBReferenceCompanies;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseUser mCurrentUser;

    private static final WMFirebase ourInstance = new WMFirebase();

    /**
     *
     * @return
     */
    public static WMFirebase getInstance() {
        return ourInstance;
    }

    /**
     *
     */
    private WMFirebase() {
        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    mCurrentUser = user;
                    opendatabase();

                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }

        };

        mAuth.addAuthStateListener(mAuthListener);
    }

    /**
     *
     */
    private void opendatabase() {
        mDatabase = FirebaseDatabase.getInstance();

        AppWorkerPresence.APP_INSTANCE.getArrayListWorkers();
        AppWorkerPresence.APP_INSTANCE.getArrayListProjectWorkers();
        AppWorkerPresence.APP_INSTANCE.getArrayListCompanyWorkers();
        AppWorkerPresence.APP_INSTANCE.getArrayListCompanyProjects();

        AppWorkerPresence.APP_INSTANCE.getArrayListWorkerProjects();
        AppWorkerPresence.APP_INSTANCE.getArrayListWorkerCompanies();

        AppWorkerPresence.APP_INSTANCE.getArrayListManagerProjects();
        AppWorkerPresence.APP_INSTANCE.getArrayListOwnerCompanies();

        AppWorkerPresence.APP_INSTANCE.getArrayListWorkTimeList();
        AppWorkerPresence.APP_INSTANCE.getArrayListLocation();

        mDBReferenceWorkers = mDatabase.getReference("workers");
        mDBReferenceWorkers.addValueEventListener(OnValueWorker);
        mDBReferenceWorkers.addChildEventListener(OnChildWorker);

        mDBReferenceWorkTimeList = mDatabase.getReference("work_time_list");
        mDBReferenceWorkTimeList.addValueEventListener(OnValueWorkTimeList);
        mDBReferenceWorkTimeList.addChildEventListener(OnChildWorkTimeList);

        mDBReferenceProjects = mDatabase.getReference("projects");
        mDBReferenceProjects.addValueEventListener(OnValueProject);
        mDBReferenceProjects.addChildEventListener(OnChildProject);

        mDBReferenceCompanies = mDatabase.getReference("companies");
        //  mDBReferenceCompanies.addValueEventListener(OnValueCompany);
        // mDBReferenceCompanies.addChildEventListener(OnChildCompany);

    }

    /**
     *
     * @return
     */
    public FirebaseUser getCurrentUser() {
        return mCurrentUser;
    }

    private ValueEventListener OnValueWorker = new ValueEventListener() {
        /**
         *
         * @param aDataSnapshot
         */
        @Override
        public void onDataChange(DataSnapshot aDataSnapshot) {
            Log.d(TAG, "OnValueWorker:onDataChange");

        }

        /**
         *
         * @param aDatabaseError
         */
        @Override
        public void onCancelled(DatabaseError aDatabaseError) {
            Log.d(TAG, "OnValueWorker:onCancelled");
        }
    };

    private ValueEventListener OnValueProject = new ValueEventListener() {
        /**
         *
         * @param dataSnapshot
         */
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            Log.d(TAG, "OnValueProject:onDataChange");
        }

        /**
         *
         * @param databaseError
         */
        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.d(TAG, "OnValueProject:onDataCancelled");
        }
    };

    private ValueEventListener OnValueWorkTimeList = new ValueEventListener() {
        /**
         *
          * @param aDataSnapshot
         */
        @Override
        public void onDataChange(DataSnapshot aDataSnapshot) {
            Log.d(TAG, "OnValueWorkTimeList:onDataChange");
        }

        /**
         *
         * @param aDatabaseError
         */
        @Override
        public void onCancelled(DatabaseError aDatabaseError) {
            Log.d(TAG, "OnValueWorkTimeList:onCancelled");
        }
    };

    private ChildEventListener OnChildWorker = new ChildEventListener() {
        /**
         *
         * @param aDataSnapshot
         * @param aS
         */
        @Override
        public void onChildAdded(DataSnapshot aDataSnapshot, String aS) {
            Log.d(TAG, "OnChildWorker:onChildAdded");
            Worker currentWorker;

            currentWorker = aDataSnapshot.getValue(Worker.class);
            AppWorkerPresence.APP_INSTANCE.getArrayListProjectWorkers().add(currentWorker);
        }

        /**
         *
         * @param aDataSnapshot
         * @param aS
         */
        @Override
        public void onChildChanged(DataSnapshot aDataSnapshot, String aS) {
            Log.d(TAG, "OnChildWorker:onChildChanged");
            Worker currentWorker;

            currentWorker = aDataSnapshot.getValue(Worker.class);
         //   AppWorkerPresence.APP_INSTANCE.getArrayListProjectWorkers().updateWorker(currentWorker);
        }

        /**
         *
         * @param aDataSnapshot
         */
        @Override
        public void onChildRemoved(DataSnapshot aDataSnapshot) {
            Log.d(TAG, "OnChildWorker:onChildRemoved");

            Worker currentWorker;

            currentWorker = aDataSnapshot.getValue(Worker.class);
         //   AppWorkerPresence.APP_INSTANCE.getArrayListProjectWorkers().removeWorker(currentWorker);
        }

        /**
         *
         * @param aDataSnapshot
         * @param aS
         */
        @Override
        public void onChildMoved(DataSnapshot aDataSnapshot, String aS) {
            Log.d(TAG, "OnChildWorker:onChildMoved");
        }

        /**
         *
         * @param aDatabaseError
         */
        @Override
        public void onCancelled(DatabaseError aDatabaseError) {
            Log.d(TAG, "OnChildWorker:onCancelled");
        }
    };

    /**
     *
     */
    private ChildEventListener OnChildProject = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot aDataSnapshot, String aS) {
            Log.d(TAG, "OnChildProject:onChildAdded");
            Project currentProject;
            currentProject = aDataSnapshot.getValue(Project.class);
            AppWorkerPresence.APP_INSTANCE.getArrayListManagerProjects().add(currentProject);
        }

        /**
         *
         * @param aDataSnapshot
         * @param s
         */
        @Override
        public void onChildChanged(DataSnapshot aDataSnapshot, String s) {
            Log.d(TAG, "OnChildProject:onChildChanged");
            Project currentProject;

            currentProject = aDataSnapshot.getValue(Project.class);
            AppWorkerPresence.APP_INSTANCE.getArrayListManagerProjects().updateProject(currentProject);
        }

        /**
         *
         * @param aDataSnapshot
         */
        @Override
        public void onChildRemoved(DataSnapshot aDataSnapshot) {
            Log.d(TAG, "OnChildProject:onChildRemoved");

            Project currentProject;

            currentProject = aDataSnapshot.getValue(Project.class);
            AppWorkerPresence.APP_INSTANCE.getArrayListManagerProjects().removeProject(currentProject);
        }

        /**
         *
         * @param aDataSnapShot
         * @param s
         */
        @Override
        public void onChildMoved(DataSnapshot aDataSnapShot, String s) {
            Log.d(TAG, "OnChildProject:onChildMoved");
        }

        /**
         *
         * @param aDatabaseError
         */
        @Override
        public void onCancelled(DatabaseError aDatabaseError) {
            Log.d(TAG, "OnChildProject:onCancelled");
        }
    };

    private ChildEventListener OnChildWorkTimeList = new ChildEventListener() {
        /**
         *
         * @param aDataSnapshot
         * @param aS
         */
        @Override
        public void onChildAdded(DataSnapshot aDataSnapshot, String aS) {
            Log.d(TAG, "OnChildWorkTimeList:onChildAdded");
            WorkTimeList currentWorkTimeList;

            currentWorkTimeList = aDataSnapshot.getValue(WorkTimeList.class);
            AppWorkerPresence.APP_INSTANCE.getArrayListWorkTimeList().add(currentWorkTimeList);
        }

        /**
         *
         * @param aDataSnapshot
         * @param aS
         */
        @Override
        public void onChildChanged(DataSnapshot aDataSnapshot, String aS) {
            Log.d(TAG, "OnChildWorkTimeList:onChildChanged");
            WorkTimeList currentWorkTimeList;

            currentWorkTimeList = aDataSnapshot.getValue(WorkTimeList.class);
            AppWorkerPresence.APP_INSTANCE.getArrayListWorkTimeList().updateWorkTimeList(currentWorkTimeList);
        }

        /**
         *
         * @param aDataSnapshot
         */
        @Override
        public void onChildRemoved(DataSnapshot aDataSnapshot) {
            Log.d(TAG, "OnChildWorkTimeList:onChildRemoved");

            WorkTimeList currentWorkTimeList;

            currentWorkTimeList = aDataSnapshot.getValue(WorkTimeList.class);
            AppWorkerPresence.APP_INSTANCE.getArrayListWorkTimeList().removeWorkTimeList(currentWorkTimeList);
        }

        /**
         *
         * @param aDataSnapshot
         * @param aS
         */
        @Override
        public void onChildMoved(DataSnapshot aDataSnapshot, String aS) {
            Log.d(TAG, "OnChildWorkTimeList:onChildMoved");
        }

        /**
         *
         * @param aDatabaseError
         */
        @Override
        public void onCancelled(DatabaseError aDatabaseError) {
            Log.d(TAG, "OnChildWorkTimeList:onCancelled");
        }
    };

    /**
     *
     * @param aWorkerUUID
     * @param aStatusNew
     */
    public void updateWorkersStatus(String aWorkerUUID, Integer aStatusNew) {
        mDBReferenceWorkers.child(aWorkerUUID).child("status").setValue(aStatusNew);
    }

    /**
     *
     * @param worker
     */
    public void addWorker(Worker worker) {
        mDBReferenceWorkers.child(worker.getDbId()).setValue(worker);
    }

    /**
     *
     * @param mCurrentTimeList
     */
    public void addWorkTimeList(WorkTimeList mCurrentTimeList) {
        mDBReferenceWorkTimeList.child(mCurrentTimeList.getDbId()).setValue(mCurrentTimeList);
    }

    /**
     *
     * @param project
     */
    public void addProject(Project project) {
        mDBReferenceProjects.child(String.valueOf(project.getDbId())).setValue(project);
    }
}
