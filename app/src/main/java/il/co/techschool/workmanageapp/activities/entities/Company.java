package il.co.techschool.workmanageapp.activities.entities;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 12/11/2017.
 */

public class Company {
    @SerializedName("db_id")
    @Expose
    private String dbId;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("owner_id")
    @Expose
    private Integer ownerId;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("company_icon")
    @Expose
    private byte[] companyIcon;
    private Context mContext;

    /**
     *
     */
    public Company() {

    }

    /**
     *
     * @param aDbId
     * @param aCompanyName
     * @param aOwnerId
     * @param aLocation
     * @param aCompanyIcon
     */
    public Company(String aDbId, String aCompanyName, Integer aOwnerId, String aLocation, byte[] aCompanyIcon) {
        dbId = aDbId;
        companyName = aCompanyName;
        ownerId = aOwnerId;
        location = aLocation;
        companyIcon =aCompanyIcon;
    }

    /**
     *
     * @return
     */
    public String getDbId() {
        return dbId;
    }

    /**
     *
     * @param aDbId
     */
    public void setDbId(String aDbId) {
        dbId = aDbId;
    }

    /**
     *
     * @return
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     *
     * @param aCompanyName
     */
    public void setCompanyName(String aCompanyName) {
        companyName = aCompanyName;
    }

    /**
     *
     * @return
     */
    public Integer getOwnerId() {
        return ownerId;
    }

    /**
     *
     * @param aOwnerId
     */
    public void setOwnerId(Integer aOwnerId) {
        ownerId = aOwnerId;
    }

    /**
     *
     * @return
     */
    public String getLocation() {
        return location;
    }

    /**
     *
     * @param aLocation
     */
    public void setLocation(String aLocation) {
        location = aLocation;
    }

    /**
     *
     * @return
     */
    public byte[] getCompanyIcon() {
        return companyIcon;
    }

    /**
     *
     * @param aCompanyIcon
     */
    public void setCompanyIcon(byte[] aCompanyIcon) {
        this.companyIcon = aCompanyIcon;
    }

    /**
     *
     * @param aCompany
     */
    public void updateFrom(Company aCompany) {
        dbId = aCompany.dbId;
        companyName = aCompany.companyName;
        ownerId = aCompany.ownerId;
        location = aCompany.location;
        companyIcon = aCompany.companyIcon;
    }

}
