package il.co.techschool.workmanageapp.activities.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import il.co.techschool.workmanageapp.activities.activities.ActFragmentJoinedCompanies;
import il.co.techschool.workmanageapp.activities.activities.ActFragmentsCreatedCompanies;

public class AdapterSectionsCompanies extends FragmentPagerAdapter {

    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String>mFragmentTitleList = new ArrayList<>();

    /**
     *
     * @param fragment
     * @param title
     */
    public void addFragment(ActFragmentsCreatedCompanies fragment, String title){
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    /**
     *
     * @param fragment
     * @param title
     */
    public void addFragment2(ActFragmentJoinedCompanies fragment, String title){
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    /**
     *
     * @param fm
     */
    public AdapterSectionsCompanies(FragmentManager fm) {
        super(fm);
    }

    /**
     *
     * @param position
     * @return
     */
    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

    /**
     *
     * @param position
     * @return
     */
    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    /**
     *
     * @return
     */
    @Override
    public int getCount() {
        return mFragmentList.size();
    }
}
