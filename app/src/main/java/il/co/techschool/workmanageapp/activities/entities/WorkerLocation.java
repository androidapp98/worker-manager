package il.co.techschool.workmanageapp.activities.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 11/16/2017.
 */

public class WorkerLocation {

    @SerializedName("db_id")
    @Expose
    private String dbid;
    @SerializedName("workerID")
    @Expose
    private Integer workerID;
    @SerializedName("longitude")
    @Expose
    private double longitude;
    @SerializedName("latitude")
    @Expose
    private double latitude;

    /**
     *
     */
    public WorkerLocation() {

    }

    /**
     *
     * @param aDbID
     * @param aWorkerID
     * @param aLongitude
     * @param aLatitude
     */
    public WorkerLocation(String aDbID, int aWorkerID, double aLongitude, double aLatitude) {
        dbid = aDbID;
        workerID = aWorkerID;
        longitude = aLongitude;
        latitude = aLatitude;
    }

    /**
     *
     * @return
     */
    public String getDbId() {
        return dbid;
    }

    /**
     *
     * @param aDbId
     */
    public void setDbId(String aDbId) {
        this.dbid = aDbId;
    }

    /**
     *
     * @return
     */
    public int getWorkerID() {
        return workerID;
    }

    /**
     *
     * @param aWorkerID
     */
    public void setWorkerID(int aWorkerID) {
        this.workerID = aWorkerID;
    }

    /**
     *
     * @return
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     *
     * @param aLongitude
     */
    public void setLongitude(double aLongitude) {
        this.longitude = aLongitude;
    }

    /**
     *
     * @return
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     *
     * @param aLatitude
     */
    public void setLatitude(double aLatitude) {
        this.latitude = aLatitude;
    }

    /**
     *
     * @param aWorkerLocation
     */
    public void updateFrom(WorkerLocation aWorkerLocation) {
        dbid = aWorkerLocation.dbid;
        workerID = aWorkerLocation.workerID;
        longitude = aWorkerLocation.longitude;
        latitude = aWorkerLocation.latitude;
    }
}