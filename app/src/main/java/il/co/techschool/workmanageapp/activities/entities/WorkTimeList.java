package il.co.techschool.workmanageapp.activities.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Admin on 10/16/2017.
 */

public class WorkTimeList {
    @SerializedName("db_id")
    @Expose
    private String dbid;
    @SerializedName("workerID")
    @Expose
    private Integer workerID;
    @SerializedName("entrance_work")
    @Expose
    private Date entranceWork;
    @SerializedName("exit_work")
    @Expose
    private Date exitWork;
    @SerializedName("enter_locationID")
    @Expose
    private Integer enterLocationID;
    @SerializedName("exit_locationID")
    @Expose
    private Integer exitLocationID;
    @SerializedName("projectID")
    @Expose
    private Integer projectID;

    /**
     *
     */
    public WorkTimeList() {
    }

    /**
     *
     * @param aWorkerID
     * @param aEntranceWork
     * @param aExitWork
     * @param aDbID
     * @param aEnterLocationID
     * @param aExitLocationID
     * @param aProjectID
     */
    public WorkTimeList(Integer aWorkerID, Date aEntranceWork, Date aExitWork,
                        String aDbID, Integer aEnterLocationID, Integer aExitLocationID, Integer aProjectID) {

        workerID = aWorkerID;
        entranceWork = aEntranceWork;
        exitWork = aExitWork;
        dbid = aDbID;
        enterLocationID = aEnterLocationID;
        exitLocationID = aExitLocationID;
        projectID = aProjectID;
    }

    /**
     *
     * @return
     */
    public String getDbId() {
        return dbid;
    }

    /**
     *
     * @param aDbId
     */
    public void setDbId(String aDbId) {
        this.dbid = aDbId;
    }

    /**
     *
     * @return
     */
    public int getWorkerID() {
        return workerID;
    }

    /**
     *
     * @param aWorkerID
     */
    public void setWorkerID(int aWorkerID) {
        this.workerID = aWorkerID;
    }

    /**
     *
     * @param aEntranceWork
     */
    public void setEntranceWork(Date aEntranceWork) {
        this.entranceWork = aEntranceWork;
    }

    /**
     *
     * @return
     */
    public Date getEntranceWork() {
        return entranceWork;
    }

    /**
     *
     * @return
     */
    public Date getExitWork() {
        return exitWork;
    }

    /**
     *
     * @param aExitWork
     */
    public void setExitWork(Date aExitWork) {
        this.exitWork = aExitWork;
    }

    /**
     *
     * @return
     */
    public int getEnterLocationID() {
        return enterLocationID;
    }

    /**
     *
     * @param aEnterLocationID
     */
    public void setEnterLocationID(int aEnterLocationID) {
        this.enterLocationID = aEnterLocationID;
    }

    /**
     *
     * @return
     */
    public int getExitLocationID() {
        return exitLocationID;
    }

    /**
     *
     * @param aExitLocationID
     */
    public void setExitLocationID(int aExitLocationID) {
        this.exitLocationID = aExitLocationID;
    }

    /**
     *
     * @return
     */
    public int getProjectID() {
        return projectID;
    }

    /**
     *
     * @param aProjectID
     */
    public void setProjectID(Integer aProjectID) {
        this.projectID = aProjectID;
    }

    /**
     *
     * @param aWorkTimeList
     */
    public void updateFrom(WorkTimeList aWorkTimeList) {
        dbid = aWorkTimeList.dbid;
        workerID = aWorkTimeList.workerID;
        entranceWork = aWorkTimeList.entranceWork;
        exitWork = aWorkTimeList.exitWork;
        enterLocationID = aWorkTimeList.enterLocationID;
        exitLocationID = aWorkTimeList.exitLocationID;
        projectID = aWorkTimeList.projectID;
    }
}
