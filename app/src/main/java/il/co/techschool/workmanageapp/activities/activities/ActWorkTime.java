package il.co.techschool.workmanageapp.activities.activities;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;

import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;

import java.lang.ref.WeakReference;
import java.util.Calendar;
import java.util.Date;

import il.co.techschool.workmanageapp.BuildConfig;
import il.co.techschool.workmanageapp.R;
import il.co.techschool.workmanageapp.activities.adapters.AdapterManagerProjectsSpinner;
import il.co.techschool.workmanageapp.activities.adapters.AdapterWorkerProjectsSpinner;
import il.co.techschool.workmanageapp.activities.common.AppWorkerPresence;
import il.co.techschool.workmanageapp.activities.entities.WorkTimeList;
import il.co.techschool.workmanageapp.activities.entities.WorkerLocation;
import il.co.techschool.workmanageapp.activities.services.LocationMonitoringService;

import static il.co.techschool.workmanageapp.activities.activities.ActLogin.PhoneNumber;


/**
 * Created by Admin on 10/13/2017.
 */
public class ActWorkTime extends AppCompatActivity implements AdapterView.OnItemSelectedListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, NavigationView.OnNavigationItemSelectedListener {

    // SideMenu
    private DrawerLayout mDrawer;
    private  NavigationView mNavigationView;
    private Button mbtnWorkTimeSignOut;

    // Start button
    private ToggleButton mtogbtnWrkTimeManagerProjects;

    private Spinner mspinnerWrkTimeManagerProjects;
    private Spinner mspinnerWrkTimeWorkerProjects;


    private TextView mtxtWrkTimeManagerProjects;
    private TextView mtxtWrkTimeWorkerProjects;

    // timer
    private TextView mtxtWrkTimeTimer;
    private TextView mtxtStatus;

    // authintication
    private FirebaseAuth mAuth;

    private Context mContext;
    private AppWorkerPresence mAppWorkerPresence;

    private Toolbar mActWorkTimeToolbar;

    private WorkTimeList mWorkTimeList;

    public int mWorkerID;
    public boolean toggleBtnState;
    public SharedPreferences preferences;
    public static final String ToggleButton = "ToggleBtn";

    private static final String TAG = ActWorkTime.class.getSimpleName();

    // for location
    /**
     * Code used in requesting runtime permissions.
     */
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;


    private boolean mAlreadyStartedService = false;
    private TextView mMsgView;

    // for timer

    private TimerService timerService;
    private boolean serviceBound;


    // Handler to update the UI every second when the timer is running
    private final Handler mUpdateTimeHandler = new UIUpdateHandler(this);

    // Message type for the handler
    private final static int MSG_UPDATE_TIME = 0;
    private String hours, minutes, seconds, milliseconds;
    private long secs, mins, hrs, msecs;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;

        initComponents();

        mAuth = FirebaseAuth.getInstance();
        mAppWorkerPresence = (AppWorkerPresence) getApplication();


        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    /**
                     *
                     * @param context
                     * @param intent
                     */
                    @Override
                    public void onReceive(Context context, Intent intent)
                    {
                        String latitude = intent.getStringExtra(LocationMonitoringService.EXTRA_LATITUDE);
                        String longitude = intent.getStringExtra(LocationMonitoringService.EXTRA_LONGITUDE);


                        if (latitude != null && longitude != null) {

                            mMsgView.setText(getString(R.string.msg_location_service_started) + "\n Latitude : " + latitude + "\n Longitude: " + longitude);

                            WorkerLocation workerLocation = new WorkerLocation();
                            double valueLat = Double.parseDouble(latitude);
                            double valueLong = Double.parseDouble(longitude);

                            workerLocation.setLatitude(valueLat);
                            workerLocation.setLongitude(valueLong);
                            workerLocation.setWorkerID(mWorkerID);
                            AppWorkerPresence.APP_INSTANCE.getWMDBAPI().saveWorkerLocation(workerLocation);
                        }
                    }
                }, new IntentFilter(LocationMonitoringService.ACTION_LOCATION_BROADCAST)
        );
    }

    /**
     *
     */
    @Override
    protected void onResume() {
        super.onResume();

        AdapterManagerProjectsSpinner adapterManagerProjectsSpinner;
        // start using the adapter after you have the data loading...
        adapterManagerProjectsSpinner = new AdapterManagerProjectsSpinner(mContext, AppWorkerPresence.APP_INSTANCE.getArrayListManagerProjects());
        mspinnerWrkTimeManagerProjects.setAdapter(adapterManagerProjectsSpinner);

        AdapterWorkerProjectsSpinner adapterWorkerProjectsSpinner;
        adapterWorkerProjectsSpinner = new AdapterWorkerProjectsSpinner(mContext, AppWorkerPresence.APP_INSTANCE.getArrayListWorkerProjects());
        mspinnerWrkTimeWorkerProjects.setAdapter(adapterWorkerProjectsSpinner);

        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        mtogbtnWrkTimeManagerProjects.setChecked(sharedPref.getBoolean(ToggleButton, toggleBtnState));
        // mWorkerID = sharedPref.getInt(WorkerId , mWorkerID2);
        initComponentListeners();

      //  checkGooglePlayServices();
    }

    /**
     *
     */
    private void initComponents() {
        setContentView(R.layout.act_work_time);

        mbtnWorkTimeSignOut = (Button) findViewById(R.id.btnWorkTimeSignOut);

        mtogbtnWrkTimeManagerProjects = (ToggleButton) findViewById(R.id.togbtnWrkTimeManagerProjects);

        mspinnerWrkTimeManagerProjects = (Spinner) findViewById(R.id.spinnerWrkTimeManagerProjects);
        mspinnerWrkTimeWorkerProjects = (Spinner) findViewById(R.id.spinnerWrkTimeWorkerProjects);

        mtxtWrkTimeManagerProjects = (TextView) findViewById(R.id.txtWrkTimeManagerProjects);
        mtxtWrkTimeWorkerProjects = (TextView) findViewById(R.id.txtWrkTimeWorkerProjects);

        mtxtWrkTimeTimer = (TextView) findViewById(R.id.txtWrkTimeTimer);
        mtxtStatus = (TextView) findViewById(R.id.txtStatus);


        mMsgView = (TextView) findViewById(R.id.txtMsgView);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarActWorkTime);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    /**
     *
     */
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /**
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * @SuppressWarnings("StatementWithEmptyBody")
     * @param item
     * @return
     */

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_projects_list) {

            Intent ActProjectsList = new Intent(ActWorkTime.this, ActTabsProjects.class);
            startActivity(ActProjectsList);
        }
        if (id == R.id.nav_companies_list) {
            Intent ActCompaniesList = new Intent(ActWorkTime.this, ActTabsCompanies.class);
            startActivity(ActCompaniesList);
        } else if (id == R.id.nav_statistics) {
            // get the user phone number
            SharedPreferences pref2 = getApplicationContext().getSharedPreferences("mypref", 0); // 0 - for private mode
            String WorkerPhone2 = pref2.getString(PhoneNumber, ""); // getting String
            // get the user id by his phone number if he did save her details
            String WorkerID2 = AppWorkerPresence.APP_INSTANCE.getWMDBAPI().loadWorkerID(WorkerPhone2);

            // load worker work time list
            Bundle dataBundle2 = new Bundle();
            dataBundle2.putString("ID", WorkerID2);
            Intent intent2 = new Intent(mContext, ActWorkTimeList.class);
            intent2.putExtras(dataBundle2);
            mContext.startActivity(intent2);

        } else if (id == R.id.nav_settings) {

        } else if (id == R.id.nav_upgrade_to_pro) {

        } else if (id == R.id.nav_share) {
            try {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, "work manage app");
                String sAux = "\nLet me recommend you this application\n\n";
                sAux = sAux + "https://play.google.com/store/apps/details?id=il.co.techschool.workmanageapp \n\n";
                i.putExtra(Intent.EXTRA_TEXT, sAux);
                startActivity(Intent.createChooser(i, "choose one"));
            } catch (Exception e) {
                //e.toString();
            }
        } else if (id == R.id.nav_rate) {
            Uri uri = Uri.parse("market://details?id=" + mContext.getPackageName());
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            // To count with Play market backstack, After pressing back button,
            // to taken back to our application, we need to add following flags to intent.
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            try {
                startActivity(goToMarket);
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id=" + mContext.getPackageName())));
            }
        } else if (id == R.id.nav_call_us) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     *
     */
    private void initComponentListeners() {

        mbtnWorkTimeSignOut.setOnClickListener(onClick);
        mtogbtnWrkTimeManagerProjects.setOnCheckedChangeListener(onCheckedChanged);
        mspinnerWrkTimeManagerProjects.setOnItemSelectedListener(this);
    }

    View.OnClickListener onClick = new View.OnClickListener() {
        /**
         *
         * @param v
         */
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnWorkTimeSignOut:
                    // logout and back to login activity
                    signOut();

                    Intent logout = new Intent(ActWorkTime.this, ActLogin.class);
                    startActivity(logout);
                    break;
            }
        }
    };


    /**
     *
     */
    // user sign out
    private void signOut() {
        mAuth.signOut();
    }


    CompoundButton.OnCheckedChangeListener onCheckedChanged = new CompoundButton.OnCheckedChangeListener() {
        /**
         *
         * @param compoundButton
         * @param isChecked
         */
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
            if (isChecked) { // toggle button is checked

                // start work
//                if (mWorkTimeList == null)
//                {
                mWorkTimeList = new WorkTimeList();
                //  }
                if (AppWorkerPresence.APP_INSTANCE.mProjectID == null) {// check project id
                    Toast errorToast = Toast.makeText(ActWorkTime.this, " Select You'r Project From the list or create new one if you don't have ", Toast.LENGTH_SHORT);
                    errorToast.show();

                    mtogbtnWrkTimeManagerProjects.setChecked(false);
                } else {

                    // start timer service
                    if (serviceBound && !timerService.isTimerRunning()) {
                        if (Log.isLoggable(TAG, Log.VERBOSE)) {
                            Log.v(TAG, "Starting timer");
                        }
                        timerService.startTimer();
                        updateUIStartRun();
                    }
                    // start location service
                    checkGooglePlayServices();

                    // get user id
                    mWorkerID = Integer.parseInt(AppWorkerPresence.APP_INSTANCE.mWorkerID);
                    // get project id
                    int projectID = Integer.parseInt(AppWorkerPresence.APP_INSTANCE.mProjectID);
                    // get time
                    Date currentStartTime = Calendar.getInstance().getTime();
                    // save
                    mWorkTimeList.setWorkerID(mWorkerID);
                    mWorkTimeList.setEntranceWork(currentStartTime);
                    mWorkTimeList.setProjectID(projectID);
                    mWorkTimeList.setExitWork(currentStartTime);

                    AppWorkerPresence.APP_INSTANCE.getWMDBAPI().saveStartWorkTime(mWorkTimeList);
                    // save toggleBtn state
                    toggleBtnState = true;
                    SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putBoolean(ToggleButton, toggleBtnState); // value to store
                    editor.apply();
                }
            } else {   // finish work..
                // finish work time

                if (AppWorkerPresence.APP_INSTANCE.mProjectID == null) {
                    Toast errorToast = Toast.makeText(ActWorkTime.this, " Select You'r Project From the list or create new one if you don't have ", Toast.LENGTH_SHORT);
                    errorToast.show();
                    mtogbtnWrkTimeManagerProjects.setChecked(false);
                } else {
                    // stop timer service
                    if (serviceBound && timerService.isTimerRunning()) {
                        if (Log.isLoggable(TAG, Log.VERBOSE)) {
                            Log.v(TAG, "Stopping timer");
                        }
                        timerService.stopTimer();
                        updateUIStopRun();
                    }
                    // stop location service
                    Intent intent = new Intent(ActWorkTime.this, LocationMonitoringService.class);
                    stopService(intent);


                    mWorkTimeList = new WorkTimeList();
                    // get time
                    Date currentFinishTime = Calendar.getInstance().getTime();
                    // get work time id
                    int workTimeID = AppWorkerPresence.APP_INSTANCE.getWMDBAPI().loadLastStartWorkTimeID(mWorkerID);

                    mWorkTimeList.setExitWork(currentFinishTime);

                    AppWorkerPresence.APP_INSTANCE.getWMDBAPI().saveFinishWorkTime(mWorkTimeList, workTimeID);

                    toggleBtnState = false;
                    SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putBoolean(ToggleButton, toggleBtnState); // value to store
                    editor.apply();

                }
            }
        }
    };


    // for spinner
    //TODO: Get project DbId her ..

    /**
     *
     * @param parent
     * @param view
     * @param pos
     * @param id
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        switch (parent.getId()) {
            case R.id.spinnerWrkTimeManagerProjects:
//                String selected = parent.getItemAtPosition(pos).toString();
//                parent.getAdapter().getItemId(pos);
//                Toast.makeText(this, selected, Toast.LENGTH_SHORT).show();


                //    mProjectID = parent.getSelectedItemPosition() + 1;

                break;

            case R.id.spinnerWrkTimeWorkerProjects:

                break;

        }

    }

    /**
     *
     * @param parent
     */
    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }


    /*
    Timer service
     */

    /**
     *
     */
    @Override
    protected void onStart() {
        super.onStart();
        if (Log.isLoggable(TAG, Log.VERBOSE)) {
            Log.v(TAG, "Starting and binding service");
        }
        Intent i = new Intent(this, ActWorkTime.TimerService.class);
        startService(i);
        bindService(i, mConnection, 0);
    }

    /**
     *
     */
    @Override
    protected void onStop() {
        super.onStop();
        updateUIStopRun();
        if (serviceBound) {
            // If a timer is active, foreground the service, otherwise kill the service
            if (timerService.isTimerRunning()) {
                timerService.foreground();
            } else {
                stopService(new Intent(this, ActWorkTime.TimerService.class));
            }
            // Unbind the service
            unbindService(mConnection);
            serviceBound = false;
        }
    }


    /**
     * Step 1: Check Google Play services
     */
    private void checkGooglePlayServices() {

        //Check whether this user has installed Google play service which is being used by Location updates.
        if (isGooglePlayServicesAvailable()) {

            //Passing null to indicate that it is executing for the first time.
            checkInternetConnection(null);

        } else {
            Toast.makeText(getApplicationContext(), R.string.no_google_playservice_available, Toast.LENGTH_LONG).show();
        }
    }


    /**
     * Step 2: Check & Prompt Internet connection
     */
    private Boolean checkInternetConnection(DialogInterface dialog) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            promptInternetConnect();
            return false;
        }


        if (dialog != null) {
            dialog.dismiss();
        }

        //Yes there is active internet connection. Next check Location is granted by user or not.

        if (checkPermissions()) { //Yes permissions are granted by the user. Go to the next step.
            startLocationService();
        } else {  //No user has not granted the permissions yet. Request now.
            requestPermissions();
        }
        return true;
    }

    /**
     * Show A Dialog with button to refresh the internet state.
     */
    private void promptInternetConnect() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ActWorkTime.this);
        builder.setTitle(R.string.title_alert_no_intenet);
        builder.setMessage(R.string.msg_alert_no_internet);

        String positiveText = getString(R.string.btn_label_refresh);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        //Block the Application Execution until user grants the permissions
                        if (checkInternetConnection(dialog)) {

                            //Now make sure about location permission.
                            if (checkPermissions()) {

                                //Step 2: Start the Location Monitor Service
                                //Everything is there to start the service.
                                startLocationService();
                            } else if (!checkPermissions()) {
                                requestPermissions();
                            }

                        }
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Step 3: Start the Location Monitor Service
     */
    private void startLocationService() {

        //And it will be keep running until you close the entire application from task manager.
        //This method will executed only once.

        if (!mAlreadyStartedService && mMsgView != null) {

            mMsgView.setText(R.string.msg_location_service_started);

            //Start location sharing service to app server.........
            Intent intent = new Intent(this, LocationMonitoringService.class);
            startService(intent);

            mAlreadyStartedService = true;
            //Ends................................................
        }
    }

    /**
     * Return the availability of GooglePlayServices
     */
    public boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(this);
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(this, status, 2404).show();
            }
            return false;
        }
        return true;
    }


    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState1 = ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);

        int permissionState2 = ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION);

        return permissionState1 == PackageManager.PERMISSION_GRANTED && permissionState2 == PackageManager.PERMISSION_GRANTED;

    }

    /**
     * Start permissions requests.
     */
    private void requestPermissions() {

        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(
                        this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION);

        boolean shouldProvideRationale2 =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION);


        // Provide an additional rationale to the img_user. This would happen if the img_user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale || shouldProvideRationale2) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            showSnackbar(R.string.permission_rationale,
                    android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(ActWorkTime.this,
                                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    });
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the img_user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(ActWorkTime.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }


    /**
     * Shows a {@link Snackbar}.
     *
     * @param mainTextStringId The id for the string resource for the Snackbar text.
     * @param actionStringId   The text of the action item.
     * @param listener         The listener associated with the Snackbar action.
     */
    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(
                findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If img_user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Log.i(TAG, "Permission granted, updates requested, starting location updates");
                startLocationService();

            } else {
                // Permission denied.

                // Notify the img_user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the img_user for permission (device policy or "Never ask
                // again" prompts). Therefore, a img_user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                showSnackbar(R.string.permission_denied_explanation,
                        R.string.settings, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
        }
    }



    // timer

    /**
     * Updates the UI when a run starts
     */
    private void updateUIStartRun() {
        mUpdateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME);
    }

    /**
     * Updates the UI when a run stops
     */
    private void updateUIStopRun() {
        mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME);
    }

    /**
     * Updates the timer readout in the UI; the service must be bound
     */
    private void updateUITimer() {
        if (serviceBound) {
            //  timerTextView.setText(timerService.elapsedTime() + " seconds");

            //    float time = System.currentTimeMillis() - startTime;
            secs = timerService.elapsedTime();
            mins = secs / 60;
            hrs = mins / 60;
            /* Convert the seconds to String
             * and format to ensure it has
             * a leading zero when required
             */
            secs = secs % 60;
            seconds = String.valueOf(secs);
            if (secs == 0) {
                seconds = "00";
            }
            if (secs < 10 && secs > 0) {
                seconds = "0" + seconds;
            }

            /* Convert the minutes to String and format the String */

            mins = mins % 60;
            minutes = String.valueOf(mins);
            if (mins == 0) {
                minutes = "00";
            }
            if (mins < 10 && mins > 0) {
                minutes = "0" + minutes;
            }

            /* Convert the hours to String and format the String */

            hours = String.valueOf(hrs);
            if (hrs == 0) {
                hours = "00";
            }
            if (hrs < 10 && hrs > 0) {
                hours = "0" + hours;
            }

            /* Although we are not using milliseconds on the timer in this example
             * I included the code in the event that you wanted to include it on your own
             */
//            milliseconds = String.valueOf((long)time);
//            if(milliseconds.length()==2){
//                milliseconds = "0"+milliseconds;
//            }
//            if(milliseconds.length()<=1){
//                milliseconds = "00";
//            }
//            milliseconds = milliseconds.substring(milliseconds.length()-3, milliseconds.length()-2);

            /* Setting the timer text to the elapsed time */
            mtxtWrkTimeTimer.setText(hours + ":" + minutes + ":" + seconds);

        }
    }

    /**
     * Callback for service binding, passed to bindService()
     */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            if (Log.isLoggable(TAG, Log.VERBOSE)) {
                Log.v(TAG, "Service bound");
            }
            ActWorkTime.TimerService.RunServiceBinder binder = (ActWorkTime.TimerService.RunServiceBinder) service;
            timerService = binder.getService();
            serviceBound = true;
            // Ensure the service is not in the foreground when bound
            timerService.background();
            // Update the UI if the service is already running the timer
            if (timerService.isTimerRunning()) {
                updateUIStartRun();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            if (Log.isLoggable(TAG, Log.VERBOSE)) {
                Log.v(TAG, "Service disconnect");
            }
            serviceBound = false;
        }
    };

    /**
     * When the timer is running, use this handler to update
     * the UI every second to show timer progress
     */
    static class UIUpdateHandler extends Handler {

        private final static int UPDATE_RATE_MS = 1000;
        private final WeakReference<ActWorkTime> activity;

        UIUpdateHandler(ActWorkTime activity) {
            this.activity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message message) {
            if (MSG_UPDATE_TIME == message.what) {
                if (Log.isLoggable(TAG, Log.VERBOSE)) {
                    Log.v(TAG, "updating time");
                }
                activity.get().updateUITimer();
                sendEmptyMessageDelayed(MSG_UPDATE_TIME, UPDATE_RATE_MS);
            }
        }
    }

    /**
     * Timer service tracks the start and end time of timer; service can be placed into the
     * foreground to prevent it being killed when the activity goes away
     */
    public static class TimerService extends Service {

        private static final String TAG = ActWorkTime.TimerService.class.getSimpleName();

        // Start and end times in milliseconds
        private long startTime, endTime;

        // Is the service tracking time?
        private boolean isTimerRunning;

        // Foreground notification id
        private static final int NOTIFICATION_ID = 1;

        // Service binder
        private final IBinder serviceBinder = new RunServiceBinder();

        public class RunServiceBinder extends Binder {
            TimerService getService() {
                return TimerService.this;
            }
        }

        /**
         *
         */
        @Override
        public void onCreate() {
            if (Log.isLoggable(TAG, Log.VERBOSE)) {
                Log.v(TAG, "Creating service");
            }
            startTime = 0;
            endTime = 0;
            isTimerRunning = false;
        }

        /**
         *
         * @param intent
         * @param flags
         * @param startId
         * @return
         */
        @Override
        public int onStartCommand(Intent intent, int flags, int startId) {
            if (Log.isLoggable(TAG, Log.VERBOSE)) {
                Log.v(TAG, "Starting service");
            }
            return Service.START_STICKY;
        }

        /**
         *
         * @param intent
         * @return
         */
        @Override
        public IBinder onBind(Intent intent) {
            if (Log.isLoggable(TAG, Log.VERBOSE)) {
                Log.v(TAG, "Binding service");
            }
            return serviceBinder;
        }

        /**
         *
         */
        @Override
        public void onDestroy() {
            super.onDestroy();
            if (Log.isLoggable(TAG, Log.VERBOSE)) {
                Log.v(TAG, "Destroying service");
            }
        }

        /**
         * Starts the timer
         */
        public void startTimer() {
            if (!isTimerRunning) {
                startTime = System.currentTimeMillis();
                isTimerRunning = true;
            } else {
                Log.e(TAG, "startTimer request for an already running timer");
            }
        }

        /**
         * Stops the timer
         */
        public void stopTimer() {
            if (isTimerRunning) {
                endTime = System.currentTimeMillis();
                isTimerRunning = false;
            } else {
                Log.e(TAG, "stopTimer request for a timer that isn't running");
            }
        }

        /**
         * @return whether the timer is running
         */
        public boolean isTimerRunning() {
            return isTimerRunning;
        }

        /**
         * Returns the  elapsed time
         *
         * @return the elapsed time in seconds
         */
        public long elapsedTime() {
            // If the timer is running, the end time will be zero
            return endTime > startTime ?
                    (endTime - startTime) / 1000 :
                    (System.currentTimeMillis() - startTime) / 1000;
        }

        /**
         * Place the service into the foreground
         */
        public void foreground() {
            startForeground(NOTIFICATION_ID, createNotification());
        }

        /**
         * Return the service to the background
         */
        public void background() {
            stopForeground(true);
        }

        /**
         * Creates a notification for placing the service into the foreground
         *
         * @return a notification for interacting with the service when in the foreground
         */
        private Notification createNotification() {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                    .setContentTitle("Timer Active")
                    .setContentText("Tap to return to the timer")
                    .setSmallIcon(R.mipmap.ic_launcher);

            Intent resultIntent = new Intent(this, ActWorkTime.class);
            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(this, 0, resultIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(resultPendingIntent);

            return builder.build();
        }
    }
    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
//    private void updateTimer (float time){
//        secs = (long)(time/1000);
//        mins = (long)((time/1000)/60);
//        hrs = (long)(((time/1000)/60)/60);
//
//        /* Convert the seconds to String
//         * and format to ensure it has
//         * a leading zero when required
//         */
//        secs = secs % 60;
//        seconds=String.valueOf(secs);
//        if(secs == 0){
//            seconds = "00";
//        }
//        if(secs <10 && secs > 0){
//            seconds = "0"+seconds;
//        }
//
//        /* Convert the minutes to String and format the String */
//
//        mins = mins % 60;
//        minutes=String.valueOf(mins);
//        if(mins == 0){
//            minutes = "00";
//        }
//        if(mins <10 && mins > 0){
//            minutes = "0"+minutes;
//        }
//
//        /* Convert the hours to String and format the String */
//
//        hours=String.valueOf(hrs);
//        if(hrs == 0){
//            hours = "00";
//        }
//        if(hrs <10 && hrs > 0){
//            hours = "0"+hours;
//        }
//
//        /* Although we are not using milliseconds on the timer in this example
//         * I included the code in the event that you wanted to include it on your own
//         */
//        milliseconds = String.valueOf((long)time);
//        if(milliseconds.length()==2){
//            milliseconds = "0"+milliseconds;
//        }
//        if(milliseconds.length()<=1){
//            milliseconds = "00";
//        }
//        milliseconds = milliseconds.substring(milliseconds.length()-3, milliseconds.length()-2);
//
//        /* Setting the timer text to the elapsed time */
//        mtxtWrkTimeTimer.setText(hours + ":" + minutes + ":" + seconds);
//      //  ((TextView)findViewById(R.id.timerMs)).setText("." + milliseconds);
//    }
//    private Runnable startTimer = new Runnable() {
//        public void run() {
//            elapsedTime = System.currentTimeMillis() - startTime;
//            updateTimer(elapsedTime);
//            mHandler.postDelayed(this,REFRESH_RATE);
//        }
//    };
//    public void startTimer (){
//        if(stopped){
//            startTime = System.currentTimeMillis() - elapsedTime;
//        }
//        else{
//            startTime = System.currentTimeMillis();
//        }
//        mHandler.removeCallbacks(startTimer);
//        mHandler.postDelayed(startTimer, 0);
//    }
//
//    public void stopTimer (){
//        mHandler.removeCallbacks(startTimer);
//        stopped = true;
//    }
//
//    public void resetTimer (){
//        stopped = false;
//        mtxtWrkTimeTimer.setText("00:00:00");
//    //    ((TextView)findViewById(R.id.timerMs)).setText(".0");
//    }

