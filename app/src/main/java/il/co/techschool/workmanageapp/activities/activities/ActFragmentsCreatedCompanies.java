package il.co.techschool.workmanageapp.activities.activities;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import il.co.techschool.workmanageapp.R;
import il.co.techschool.workmanageapp.activities.adapters.AdapterOwnerCompanies;
import il.co.techschool.workmanageapp.activities.common.AppWorkerPresence;

public class ActFragmentsCreatedCompanies extends Fragment {

    private static final String TAG = "CreatedCompanies";
    private AppWorkerPresence mAppWorkerPresence;
    private ListView mlstFragmentCreatedCompanies;

    /**
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_created_companies,container,false);

        mlstFragmentCreatedCompanies = (ListView)view.findViewById(R.id.lstFragmentCreatedCompanies);

        AdapterOwnerCompanies adapterOwnerCompanies;
        adapterOwnerCompanies = new AdapterOwnerCompanies(getActivity(), AppWorkerPresence.APP_INSTANCE.getArrayListOwnerCompanies());

        mlstFragmentCreatedCompanies.setAdapter(adapterOwnerCompanies);

        return view;
    }
}
