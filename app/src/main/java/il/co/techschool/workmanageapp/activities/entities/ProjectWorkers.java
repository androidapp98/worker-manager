package il.co.techschool.workmanageapp.activities.entities;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 12/2/2017.
 */

public class ProjectWorkers {
    @SerializedName("db_id")
    @Expose
    private String dbId;
    @SerializedName("project_id")
    @Expose
    private Integer projectID;
    @SerializedName("worker_id")
    @Expose
    private Integer workerID;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("salary")
    @Expose
    private Integer salary;

    private Context mContext;

    /**
     *
     */
    public ProjectWorkers() {

    }

    /**
     *
     * @param aDbId
     * @param aProjectID
     * @param aWorkerID
     * @param aStatus
     * @param aSalary
     */
    public ProjectWorkers(String aDbId, Integer aProjectID, Integer aWorkerID, Integer aStatus, Integer aSalary) {

        dbId = aDbId;
        projectID = aProjectID;
        workerID = aWorkerID;
        status = aStatus;
        salary = aSalary;
    }

    /**
     *
     * @return
     */
    public String getDbId() {
        return dbId;
    }

    /**
     *
     * @param aDbID
     */
    public void setDbId(String aDbID) {
        dbId = aDbID;
    }

    /**
     *
     * @return
     */
    public Integer getProjectID() {
        return projectID;
    }

    /**
     *
     * @param aProjectID
     */
    public void setProjectID(Integer aProjectID) {
        projectID = aProjectID;
    }

    /**
     *
     * @return
     */
    public Integer getWorkerID() {
        return workerID;
    }

    /**
     *
     * @param aWorkerID
     */
    public void setWorkerID(Integer aWorkerID) {
        workerID = aWorkerID;
    }

    /**
     *
     * @return
     */
    public Integer getStatus() {
        return status;
    }

    /**
     *
     * @param aStatus
     */
    public void setStatus(Integer aStatus) {
        status = aStatus;
    }

    /**
     *
     * @return
     */
    public Integer getSalary() {
        return salary;
    }

    /**
     *
     * @param aSalary
     */
    public void setSalary(Integer aSalary) {
        salary = aSalary;
    }

    /**
     *
     * @param aProjectWorkers
     */
    public void updateFrom(ProjectWorkers aProjectWorkers) {
        dbId = aProjectWorkers.dbId;
        projectID = aProjectWorkers.projectID;
        workerID = aProjectWorkers.workerID;
        status = aProjectWorkers.status;
        salary = aProjectWorkers.salary;
    }
}
